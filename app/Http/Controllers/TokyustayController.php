<?php

namespace App\Http\Controllers;
use App\Models\Hotel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Controllers\Controller;
use App\Http\RouteBuilder;
use Request;
use Response;
use Storage;
use App\Http\Controllers\ExcelReviser;
use DB;

class TokyustayController extends Controller
{
    private static $customer_company_seq = 4633;
    private static $new_customer_company_seq = 179;
    private static $allCnt;
    private static $sel_keyword_mst_seq = array();
    private static $kuchikomiCnt = 0;
    private static $sites = array('rakuten','jalan','rurubu','tripadvisor','agoda','expedia','booking');
    
    public static function getRankingSites(){
        $arr = array('rakuten'=>'楽天トラベル','jalan'=>'じゃらん','rurubu'=>'るるぶトラベル','tripadvisor'=>'トリップアドバイザー','expedia'=>'エクスペディア','booking'=>'booking.com','agoda'=>'Agoda',);
        return $arr;
    }
    
    public static function  getSex(){
        $arr = array('male'=>'男性','female'=>'女性');
        return $arr;
    }
    
    public static function  getHotelCol(){
        return array('rakuten'=>'f_hotel_no','jalan'=>'yado_cd','rurubu'=>'yado_cd','tripadvisor'=>'d_cd','expedia'=>'hotel_cd','booking'=>'hotelId','agoda'=>'hotel_id',);
    }
    
    public static function Check(){
        echo "hello tokyustay customize api";
        exit;
    }
    
    public static function  getAreaSiteHotelCode($sel_keymst_seq = array(),$site_id,$large_area_code){
        
        $site_hotel_col = self::getHotelCol();
        
        $query = DB::connection('reviewM')->table('rep_'.$site_id.'_mst as S')
                    ->leftjoin('keyword_mst as K', function($query) {
                        $query->on('S.repchecker_keyword_mst_seq', '=', 'K.seq');
                    });
                    $query->leftjoin('hotel_details as H', function($query) {
                        $query->on('H.keyword_mst_seq', '=', 'K.seq');
                    });
        $query->where([
                        ['K.business_status', '=', '0'],
                        [$site_hotel_col[$site_id], '!=', ''],
                        ['repchecker_keyword_mst_seq', '!=', '0'],
                ]);
                
        $query->whereRaw("(K.keyword IS NOT NULL AND K.keyword != '')");
        $query->whereRaw("(H.ps_hotel_genre = '3' OR H.ps_hotel_genre = '6')");
        $query->whereRaw(" area_large_area_cd IN (".$large_area_code.") ");

        $reseult = $query->pluck($site_hotel_col[$site_id]);

        $reseult = json_decode(json_encode($reseult), true);
        
        return $reseult;
    }

    public static function  getAreaKeywordSeqs($sel_keymst_seq = array(),$site_id,$large_area_code){
        
        $query = DB::connection('reviewM')->table('keyword_mst as K')
                    ->leftjoin('hotel_details as H', function($query) {
                        $query->on('H.keyword_mst_seq', '=', 'K.seq');
                    });
        $query->where([
                        ['K.business_status', '=', '0'],
                ]);
                
        $query->whereRaw("(K.keyword IS NOT NULL AND K.keyword != '')");
        $query->whereRaw("(H.ps_hotel_genre = '3' OR H.ps_hotel_genre = '6')");
        $query->whereRaw(" area_large_area_cd IN (".$large_area_code.") ");

        $reseult = $query->pluck("H.keyword_mst_seq");

        $reseult = json_decode(json_encode($reseult), true);
        
        return $reseult;
    }

    
    public static function getKuchikomiDetails($site_id,$kuchikomi_seq_list){
        
        if(!in_array($site_id, static::$sites)){
            return array();
        }
        
        $query = DB::connection('review')->table($site_id.'_kuchikomi');
        $query->whereIn('id',$kuchikomi_seq_list);
        
        $resTMP = $query->get();
        $resTMP = json_decode(json_encode($resTMP), true);
        
        $res = array();
        foreach ($resTMP as $k => $v) {
            $res[$k] = $v;
            $res[$k]['seq'] = $v['id'];
        }
        
        return $res;
    }
    
    public static function  getReptation(){
        $arr = array(           'rakuten'=>array('total'=>'総合','service'=>'サービス','location'=>'立地','room'=>'部屋','amenity'=>'設備・アメニティ','bathroom'=>'風呂','food'=>'食事',),
                                'jalan'=>array('total'=>'総合','room'=>'部屋','bathroom'=>'風呂','breakfast'=>'料理（朝食）','dinner'=>'料理（夕食）','service'=>'接客・サービス','clean'=>'清潔感',),
                                'rurubu'=>array('total'=>'総合','room'=>'部屋','service'=>'サービス','food'=>'食事','location'=>'立地','amenity'=>'設備','bathroom'=>'風呂',),
                                'tripadvisor'=>array('total'=>'総合','location'=>'立地','bedroom'=>'寝心地','room'=>'客室','service'=>'サービス','price'=>'価格','clean'=>'清潔感',),
                                'expedia'=>array('total'=>'総合','clean'=>'清潔感','service'=>'サービス','room'=>'客室','conditions'=>'設備','location'=>'立地','satisfaction'=>'ひと休み度',),
                                'booking'=>array('total'=>'総合','clean'=>'clean','comfort'=>'comfort','location'=>'location','amenity'=>'amenity','staff'=>'staff','value'=>'value','free_wifi'=>'wifi',),
                                'agoda'=>array('total'=>'総合','price'=>'price','location'=>'location','staff'=>'staff','clean'=>'clean','room'=>'room','food'=>'food','amenity'=>'amenity',),
                                );
        return $arr;
    }
    
    public static function  getHotelName($keyword_mst_seq){
        
        $query = DB::connection('reviewM')->table('hotel_details');
        $query->select(
                    'hotel_name'
                    );
        $query->where('keyword_mst_seq',$keyword_mst_seq);
        $res = $query->pluck('hotel_name');
        $res = json_decode(json_encode($res), true);
        
        $data = "";
        if(isset($res[0])){
            $data = $res[0];
        }
        
        return $data;
    }
    
    public static function  getSelectSiteHotelCode($site_id){
        
        $site_hotel_col = self::getHotelCol();
        
        $query = DB::connection('reviewM')->table('rep_'.$site_id.'_mst as S')
                    ->leftjoin('keyword_mst as K', function($query) {
                        $query->on('S.repchecker_keyword_mst_seq', '=', 'K.seq');
                    });
                    $query->leftjoin('hotel_details as H', function($query) {
                        $query->on('H.keyword_mst_seq', '=', 'K.seq');
                    });
        //$query->select($site_hotel_col[$site_id]);
        $query->where([
                        ['K.business_status', '=', '0'],
                        [$site_hotel_col[$site_id], '!=', ''],
                        ['repchecker_keyword_mst_seq', '!=', '0'],
                ]);
        $query->whereRaw("(K.keyword IS NOT NULL AND K.keyword != '')");
        $query->whereRaw("(H.ps_hotel_genre = '3' OR H.ps_hotel_genre = '6')");
        
        $reseult = $query->pluck($site_hotel_col[$site_id]);
        $reseult = json_decode(json_encode($reseult), true);
        
        return $reseult;
    }
    
    public static function  getAllSiteHotelCode($site_id){
        
        $site_hotel_col = self::getHotelCol();
        
        $query = DB::connection('reviewM')->table('rep_'.$site_id.'_mst as S')
                    ->leftjoin('keyword_mst as K', function($query) {
                        $query->on('S.repchecker_keyword_mst_seq', '=', 'K.seq');
                    });
                    $query->leftjoin('hotel_details as H', function($query) {
                        $query->on('H.keyword_mst_seq', '=', 'K.seq');
                    });
        //$query->select($site_hotel_col[$site_id]);
        $query->where([
                        ['K.business_status', '=', '0'],
                        [$site_hotel_col[$site_id], '!=', ''],
                        ['repchecker_keyword_mst_seq', '!=', '0'],
                ]);
        $query->whereRaw("(K.keyword IS NOT NULL AND K.keyword != '')");
        $query->whereRaw("(H.ps_hotel_genre = '3' OR H.ps_hotel_genre = '6')");
        
        $reseult = $query->pluck($site_hotel_col[$site_id]);
        $reseult = json_decode(json_encode($reseult), true);
        
        return $reseult;
    }
    
    // 評価点数取得
    public static function  getReptationRuikeiGroupData($group = '', $site_id = '',$site_hotel_cd = '', $t_site_hotel_cd=array(), $sel_site_hotel_cd=array(), $all_site_hotel_cd=array(), $r_group_seq=array()){
        
        $site_col = self::getReptationTotal();
        $site_hotel_col = self::getHotelCol();
        $site_info = self::getRankingSites();
        
        if($group == 'brand'){
            $group_cnt = self::getBrandHotelCntData();
        }elseif($group == 'chain'){
            $group_cnt = self::getChainHotelCntData();
        }
         
        $data = array();
        $rep_arr_tmp = array();
        foreach($site_col[$site_id] AS $col => $val){
            $rep_arr_tmp[] .= " (".$col." ) AS ".$col."_sum ";
        }
        
        $sql_str = implode(",",$rep_arr_tmp);
        
        
        // 媒体でホテルコードがバラバラ
        if($site_id == 'ikyu' || $site_id == 'ikyubiz'){
            $yado_cd = 'dealid';
        }else if($site_id == 'benefitone'){
            $yado_cd = 'menu_no';
        }else if($site_id == 'agoda'){
            $yado_cd = 'hotel_id';
        }else if($site_id == 'booking'){
            $yado_cd = 'hotelId';
        }else if($site_id == 'expedia'){
            $yado_cd = 'hotel_cd';
        }else{
            $yado_cd = 'yado_cd';
        }
        
        
        if($group == 'brand'){
            $seq_list = self::getBrandHotelKeywordMstSeq();
            $site_seq_list = self::getBrandHotelSiteCode($site_id,$site_hotel_col[$site_id]);
        }elseif($group == 'chain'){
            $seq_list = self::getChainHotelKeywordMstSeq();
            $site_seq_list = self::getChainHotelSiteCode($site_id,$site_hotel_col[$site_id]);
        }
        
        $chain_list = self::getTokyustayChainList();
        $tokyu_keyword_seqs = array();
        foreach ($chain_list as $k => $v) {
            $tokyu_keyword_seqs[] = $k;
        }
        
        $start_date = date("Y-m-d");
        //TODO
        $start_date = "2019-03-01";
        // 仕方なく3回ぶん回す　あとで合算してる
        $table_set = array(1,2,3);
        
        $sql_str = " keyword_mst_seq,";
        $sql_str .= " kuchikomi_cnt,";
        $sql_str .= implode(",",$rep_arr_tmp);
        
        $query = DB::connection('reputation')->table("rep_".$site_id."_reptation");
        $query->selectRaw(
                $sql_str
                ); 
        $query->whereIn('keyword_mst_seq',$tokyu_keyword_seqs); 
        $query->where('date',$start_date);
        
        $reseult = $query->get();
        $reseult = json_decode(json_encode($reseult), true);
        
        $sql_str .=  " hotel_chain.name AS group_name, ";
        $sql_str .=  " hotel_chain.seq AS group_seq ";
        
        $data = array();
        foreach ($reseult as $k => $v) {
            $data[$k] = $v;
            if($chain_list[$v['keyword_mst_seq']]){
                $data[$k]['group_name'] = $chain_list[$v['keyword_mst_seq']]['group_name'];
                $data[$k]['group_seq'] = $chain_list[$v['keyword_mst_seq']]['group_seq'];
                $data[$k]['type'] = "chain";
            }else{
                $data[$k]['group_name'] = "";
                $data[$k]['group_seq'] = "";
                $data[$k]['type'] = "chain";
            }
            
        }
        
       
        if(count($data) == 0){
            return $data;
        }
        
        $group_cnt_list = array();
        foreach($data as $key => $val){
            if(!isset($group_cnt_list[$val['group_seq']])){
                $group_cnt_list[$val['group_seq']] = 1;
            }else{
                $group_cnt_list[$val['group_seq']] += 1;
            }
        
        }

        foreach($data as $key => $val){
            $data[$key]['group_seq_cnt'] = $group_cnt_list[$val['group_seq']];
        }
        
        // 集計
        $data_tmp = $data;
        unset($data);
        $data = array();
        foreach($data_tmp AS $k => $site_data){
            if(!isset($site_data['group_seq'])){
                continue;
            }
            if(!isset($group_cnt[$site_data['group_seq']])){
                continue;
            }
            
            $data[$site_data['group_seq']]['hotel_cnt'] = $group_cnt[$site_data['group_seq']];
            $data[$site_data['group_seq']]['type'] = $site_data['type'];
            $data[$site_data['group_seq']]['group_seq'] = $site_data['group_seq'];
            $data[$site_data['group_seq']]['group_name'] = $site_data['group_name'];
            $data[$site_data['group_seq']]['group_seq_cnt'] = $site_data['group_seq_cnt'];
            
            if(!isset($data[$site_data['group_seq']]['kuchikomi_cnt'])){
                $data[$site_data['group_seq']]['kuchikomi_cnt'] = 0;
            }
            $data[$site_data['group_seq']]['kuchikomi_cnt'] += $site_data['kuchikomi_cnt'];
            
            foreach($site_col[$site_id] AS $col => $val){
                if(!isset($data[$site_data['group_seq']][$col.'_sum'])){
                    $data[$site_data['group_seq']][$col.'_sum'] = 0;
                }
                $data[$site_data['group_seq']][$col.'_sum'] += $site_data[$col.'_sum'];
            }
        }
        
        if(count($data) == 0){
            return $data;
        }
        
        // データ整形＆条件指定
        $data_tmp = $data;
        unset($data);
        
        $data_tmp2 = array();
        
        foreach($data_tmp AS $k => $site_data){

            $data[$site_data['group_seq']]['hotel_cnt'] = $site_data['hotel_cnt'];
            $data[$site_data['group_seq']]['type'] = $site_data['type'];
            $data[$site_data['group_seq']]['group_seq'] = $site_data['group_seq'];
            $data[$site_data['group_seq']]['group_name'] = $site_data['group_name'];
            $data[$site_data['group_seq']]['kuchikomi_cnt'] = $site_data['kuchikomi_cnt'];
            foreach($site_col[$site_id] AS $col => $val){
                if(!isset($data[$site_data['group_seq']][$col])){
                    $data[$site_data['group_seq']][$col] = 0;
                }
                $data[$site_data['group_seq']][$col] = round(($site_data[$col.'_sum'] / $site_data['group_seq_cnt']),2);
            }
        }
        //東急ステイ
        return $data['93'];
    }

    // 評価点数取得
    public static function  getReptationRuikeiAllData($data_type = 'row',$site_id = '',$t_site_hotel_cd = array(),$area_code =''){
        
        $site_col = self::getReptationTotal();
        
        $date = date("Y-m-d");
        $rep_arr_tmp = array();
        foreach($site_col[$site_id] AS $col => $val){
            $rep_arr_tmp[] .= "ROUND(AVG(IF (".$col."!=0,".$col.",null)),2) AS ".$col." ";
        }
        $sql_str = implode(",",$rep_arr_tmp);
        
        $query = DB::connection('reputation')->table("rep_".$site_id."_reptation");
        $query->selectRaw(
                $sql_str
                ); 
        $query->whereIn('keyword_mst_seq',$t_site_hotel_cd);
        $query->where('date',$date); 
        
        $reseult = $query->get();
        $reseult = json_decode(json_encode($reseult), true);
        
        return $reseult[0];
    }
    
    public static function getHotelEffectCountByKuchikomiScoreValuechain($company_seq,$date_from,$date_to,$keyword_mst_seq){
        
        
        $vc_mst_seqs = self::getDictionarySeq($company_seq,$keyword_mst_seq);
        
        $analyze_result = self::getVcAnalyzeResult($company_seq,$vc_mst_seqs,$date_from,$date_to);
        
        //クチコミ件数を0で初期設定
        static::$kuchikomiCnt = 0;
        
        $all_keyword_list = self::getAccountKeywordsAll($company_seq, $keyword_mst_seq, $vc_mst_seqs);
        
        
        $act_list = array();
        $act_name = array();
        $comp_name = array();

        foreach ($all_keyword_list as $key_1 => $val_1) {
            $act_list[$val_1['comp_kwd_name']] =$val_1['act_seq'];
            $act_list[$val_1['comp_name']] = $val_1['act_seq'];
            $act_name[$val_1['act_seq']] = $val_1['act_name'];
            
            $comp_name[$val_1['comp_kwd_name']] = $val_1['comp_seq'];
            $comp_name[$val_1['comp_name']] = $val_1['comp_seq'];
        }
        
        
        $tmp = 0;
        foreach ($analyze_result as $a_k => $a_v) {
            
            $table = "vctm_vc_analyze_result_4633";
        
            $query = DB::connection('tokyustay')->table($table);
            $query->select(
                        'vc_kind',
                        'vc_kw'
                        
                        );            
            $query->where([
                            ['company_seq', '=', '4633'],
                            ['vc_kind', '!=', 'naga_posi_list'],
                            ['mng_kuchikomi_mst_seq',  $a_v['mng_kuchikomi_mst_seq']],
                            ['sentence_cnt',  $a_v['sentence_cnt']],
                    ]);
            //$query->whereRaw("lang IN ('','ja')");
            $query->whereIn('vc_mst_seq',$vc_mst_seqs);

            $com_suff = $query->groupBy('vc_kind')->orderByRaw('post_date DESC')->get()->toarray();
            $com_suff = json_decode(json_encode($com_suff), true);
            
            //$sql = " SELECT vc_kind,vc_kw ";
            //$sql.= " FROM vctm_vc_analyze_result_".$company_seq;
            //$sql.= " WHERE vc_mst_seq IN (".implode(",",$vc_mst_seqs).") ";
            //$sql.= " AND vc_kind != 'naga_posi_list' ";
            //$sql.= " AND mng_kuchikomi_mst_seq = '".$a_v['mng_kuchikomi_mst_seq']."' ";
            //$sql.= " AND sentence_cnt = '".$a_v['sentence_cnt']."' ";
            //$sql.= " GROUP BY vc_kind ";
            //$sql.= " ORDER BY post_date desc ";
            //echo $sql;exit;
            //$com_suff = SQL::selData(self::_lib,'vctm_repchecker',$sql,'All',__FILE__,__FUNCTION__,__LINE__);
            
            foreach ($com_suff as $c_k => $c_v) {
                if($c_v['vc_kind'] == 'comp_kw'){
                    //$a_v['main_or_support'] = 'main';
                    
                    if(isset($act_list[$c_v['vc_kw']])){
                        $a_v['act_seq'] = $act_list[$c_v['vc_kw']];
                        $a_v['act_name'] = $act_name[$a_v['act_seq']];
                        $a_v['comp_name'] = $c_v['vc_kw'];
                        $a_v['comp_seq'] = $comp_name[$a_v['comp_name']];
                    }
                    
                }
               
            }//end of foreach

            if(isset($a_v['act_seq'])){
                $return_arr[] = $a_v;
            }
            
        }

        
        $mng_kuchikomi_seq_convert_arr = array();
        foreach ($analyze_result as $k => $v) {
            $mng_kuchikomi_seq_convert_arr[$v['site_id']][] = $v['mng_kuchikomi_mst_seq'];
        }
        
        $match_info_seq_arr = array();
        foreach ($mng_kuchikomi_seq_convert_arr as $site_id => $mng_kuchikomi_seq_list) {
            $match_info_seq_arr[$site_id] = self::getPairMngKuchikomiSeqNkuchikomiSeq($site_id,$mng_kuchikomi_seq_list);
        }
        
        $kuchikomi_seq_arr = array();
        $convert_mngseq_to_kuchikomi_seq = array();
        foreach ($match_info_seq_arr as $site_id => $seq_infos) {
            foreach ($seq_infos as $no => $seq_info) {
                $kuchikomi_seq_arr[$site_id][] = $seq_info['kuchikomi_seq'];
                $convert_mngseq_to_kuchikomi_seq[$site_id][$seq_info['kuchikomi_seq']] = $seq_info['mng_kuchikomi_seq'];
            }
        }
        
        $kuchikomi_details = array();
        foreach ($kuchikomi_seq_arr as $site_id => $kuchikomi_seq_list) {
            $kuchikomi_details[$site_id] = self::getKuchikomiDetails($site_id,$kuchikomi_seq_list);
        }
        
        
        $input_kuchikomi_details = array();
        foreach ($kuchikomi_details as $site_id => $details) {
            foreach ($details as $k => $v) {
                if(isset($convert_mngseq_to_kuchikomi_seq[$site_id][$v['seq']])){
                    $mng_kuchikomi_seq = $convert_mngseq_to_kuchikomi_seq[$site_id][$v['seq']];
                    $input_kuchikomi_details[$mng_kuchikomi_seq] = $v;
                }   
                
            }
        }
        $result_tmp = array();
        $count = 0 ;
        foreach ($analyze_result as $k => $v) {
            if(isset($input_kuchikomi_details[$v['mng_kuchikomi_mst_seq']])){
                /*$result_tmp[$count] = $v;
                
                foreach ($input_kuchikomi_details[$v['mng_kuchikomi_mst_seq']] as $key => $value) {
                    $result_tmp[$count][$key] = $value;
                }*/
                $result_tmp[$count] = array_merge($v,$input_kuchikomi_details[$v['mng_kuchikomi_mst_seq']]);
                $count++;
            }
        }
        //TODO 条件作成
        
        $grouping_arr = self::getGroupingDataAll($company_seq,$keyword_mst_seq,$vc_mst_seqs);
        
        $result = array();

        $result_tmp_num = 0;
        //act nameを追加
        foreach ($result_tmp as $key => $value) {
                
                $result[$result_tmp_num] = $value;
                if(isset($grouping_arr[$value['vc_kw']]))
                    $group = $grouping_arr[$value['vc_kw']];
                    $result[$result_tmp_num]['grouping'] = $group;
                
                $result_tmp_num++;
        }
        
        
        $return = array();
        $k = 0;
        if(!isset($return_arr)) return ;
        for($i = 0 ; $i < count($return_arr) ; $i++){
            for($z = 0 ; $z < count($result) ; $z++){
                
                if(isset($result[$z]['mng_kuchikomi_mst_seq']) && isset($result[$z]['grouping']) &&  $result[$z]['grouping'] != 'independent' && 
                    $return_arr[$i]['mng_kuchikomi_mst_seq'] == $result[$z]['mng_kuchikomi_mst_seq']){
                        $return[$k] = $result[$z];
                        $return[$k]['act_name'] = $return_arr[$i]['act_name'];
                        $return[$k]['act_seq']  = $return_arr[$i]['act_seq'];
                        $k++;
                }
            }   
             
        }
        
        $response = array();
        $req_cnt  =0;
        $posi_cnt =0;
        $nega_cnt =0;
        
        for($i = 0 ; $i < count($return) ; $i++){
            $img_flg = 0;
            if(isset($return[$i]['total'])){
                if(!preg_match("/[(0-9)+]/", $return[$i]['site_id'])){
                    $img_flg = 1;
                }
                
                $return[$i]['img_flg'] = $img_flg;
                
                
                if($return[$i]['grouping'] == 'request'){
                    $response['request'][$req_cnt] = $return[$i];
                    $req_cnt++;
                }
                
                if($return[$i]['site_id'] == 'booking'){
                    if($return[$i]['grouping'] == 'positive' && $return[$i]['total'] >= 8){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && $return[$i]['total'] < 8){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }
                }else if(isset($return[$i]['act_name']) && ($return[$i]['act_name'] == '【サービス】' || $return[$i]['act_name'] == '【チェックIN/OUT】'
                || $return[$i]['act_name'] == 'アフタフォロー'|| $return[$i]['act_name'] == 'サービス'|| $return[$i]['act_name'] == 'スタッフ一般'
                || $return[$i]['act_name'] == 'チェックアウト'|| $return[$i]['act_name'] == 'チェックイン'|| $return[$i]['act_name'] == 'フロント'
                || $return[$i]['act_name'] == 'マナー'|| $return[$i]['act_name'] == '予約'|| $return[$i]['act_name'] == '施設・その他サービス'
                || $return[$i]['act_name'] == '注文'|| $return[$i]['act_name'] == '笑顔'|| $return[$i]['act_name'] == '笑顔'
                || $return[$i]['act_name'] == 'サービス一般'|| $return[$i]['act_name'] == 'フロント対応'|| $return[$i]['act_name'] == 'ルームサービス'
                || $return[$i]['act_name'] == 'レストランサービス')){

                    if($return[$i]['grouping'] == 'positive' && isset($return[$i]['service']) && $return[$i]['service'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['service']) && $return[$i]['service'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['service']) || $return[$i]['service'] == 0) && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && ( !isset($return[$i]['service']) ||$return[$i]['service'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }
                        
                }else if(isset($return[$i]['act_name']) && ($return[$i]['act_name'] == '【夕食】' || $return[$i]['act_name'] == '【朝食】'
                || $return[$i]['act_name'] == 'お茶'|| $return[$i]['act_name'] == 'コーヒー'|| $return[$i]['act_name'] == '味'
                || $return[$i]['act_name'] == '夕食'|| $return[$i]['act_name'] == '早餐'|| $return[$i]['act_name'] == '昼食'
                || $return[$i]['act_name'] == '晚餐'|| $return[$i]['act_name'] == '朝食')){
                        
                    //positive
                    if($return[$i]['grouping'] == 'positive' && isset($return[$i]['food']) && $return[$i]['food'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && isset($return[$i]['dinner']) && $return[$i]['dinner'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && isset($return[$i]['breakfast']) && $return[$i]['breakfast'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['food']) || $return[$i]['food'] == 0) && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive'  && (!isset($return[$i]['dinner']) || $return[$i]['dinner'] == 0)  && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['breakfast']) || $return[$i]['breakfast'] == 0)  && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }
                    
                    //negative
                    if($return[$i]['grouping'] == 'negative' && isset($return[$i]['food']) && $return[$i]['food'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['dinner']) && $return[$i]['dinner'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['breakfast']) && $return[$i]['breakfast'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['food']) || $return[$i]['food'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['dinner']) || $return[$i]['dinner'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['breakfast']) || $return[$i]['breakfast'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }
                    
                }else if(isset($return[$i]['act_name']) && ($return[$i]['act_name'] == '【部屋】' || $return[$i]['act_name'] == 'ベッド'
                || $return[$i]['act_name'] == '客室'|| $return[$i]['act_name'] == '客室関連'|| $return[$i]['act_name'] == '房間'
                || $return[$i]['act_name'] == '清潔'|| $return[$i]['act_name'] == '睡眠'|| $return[$i]['act_name'] == '部屋'
                || $return[$i]['act_name'] == 'ベッドルーム')){
                    
                    if($return[$i]['grouping'] == 'positive' && isset($return[$i]['room']) && $return[$i]['room'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && isset($return[$i]['bedroom']) && $return[$i]['bedroom'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['room']) || $return[$i]['room'] == 0)  && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['bedroom']) || $return[$i]['bedroom'] == 0)  && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }
                    
                    if($return[$i]['grouping'] == 'negative' && isset($return[$i]['room']) && $return[$i]['room'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['bedroom']) && $return[$i]['bedroom'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['room']) || $return[$i]['room'] == 0) &&  $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['bedroom']) || $return[$i]['bedroom'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }
                }else if(isset($return[$i]['act_name']) && ($return[$i]['act_name'] == '【風呂】' || $return[$i]['act_name'] == 'シャンプー'
                || $return[$i]['act_name'] == 'バスルーム'|| $return[$i]['act_name'] == '個室露天風呂'|| $return[$i]['act_name'] == '大浴場'
                || $return[$i]['act_name'] == '沐浴'|| $return[$i]['act_name'] == '温泉'|| $return[$i]['act_name'] == '温泉・大浴場'
                || $return[$i]['act_name'] == '風呂')){
                
                
                    if($return[$i]['grouping'] == 'positive' && isset($return[$i]['bathroom']) && $return[$i]['bathroom'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['bathroom']) && $return[$i]['bathroom'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['bathroom']) || $return[$i]['bathroom'] == 0) && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['bathroom']) || $return[$i]['bathroom'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }

                }else if(isset($return[$i]['act_name']) && ($return[$i]['act_name'] == 'ひげそり' || $return[$i]['act_name'] == 'アメニティ'
                || $return[$i]['act_name'] == 'タオル'|| $return[$i]['act_name'] == 'パジャマ'|| $return[$i]['act_name'] == '歯ブラシ'
                || $return[$i]['act_name'] == '石鹸'|| $return[$i]['act_name'] == '客室アメニティ'|| $return[$i]['act_name'] == '客室備品')){
                
                
                    if($return[$i]['grouping'] == 'positive' && isset($return[$i]['amenity']) && $return[$i]['amenity'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['amenity']) && $return[$i]['amenity'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'positive' && (!isset($return[$i]['amenity']) || $return[$i]['amenity'] == 0) && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['amenity']) || $return[$i]['amenity'] == 0) && $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }
                
                }else if(isset($return[$i]['act_name']) && ($return[$i]['act_name'] == '清掃' || $return[$i]['act_name'] == '清掃状態・清潔度'
                || $return[$i]['act_name'] == '清潔')){
                    
                    
                    if($return[$i]['grouping'] == 'positive' && isset($return[$i]['clean']) && $return[$i]['clean'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && isset($return[$i]['clean']) && $return[$i]['clean'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }else if($return[$i]['grouping'] == 'positive'  && (!isset($return[$i]['clean']) || $return[$i]['clean'] == 0) && $return[$i]['total'] >= 4){
                        $response['positive'][$posi_cnt] = $return[$i];
                        $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' && (!isset($return[$i]['clean']) || $return[$i]['clean'] == 0) &&  $return[$i]['total'] <= 3){
                        $response['negative'][$nega_cnt] = $return[$i];
                        $nega_cnt++;
                    }
                }else{
                    if($return[$i]['grouping'] == 'positive'   && $return[$i]['total'] >= 4){
                            $response['positive'][$posi_cnt] = $return[$i];
                            $posi_cnt++;
                    }else if($return[$i]['grouping'] == 'negative' &&  $return[$i]['total'] <= 3){
                            $response['negative'][$nega_cnt] = $return[$i];
                            $nega_cnt++;
                    }
                }
            }
        }
        
        return $response;
    }

    // 評価点数取得
    public static function  getReptationRuikeiData($site_id = '', $site_hotel_cd = '', $t_site_hotel_cd=array(), $sel_site_hotel_cd=array(), $all_site_hotel_cd=array(), $r_site_hotel_cd=array(), $large_area_code =""){
        
        $site_col = self::getReptationTotal();
        $site_hotel_col = self::getHotelCol();
        $site_info = self::getRankingSites();
        $sex_type = self::getSex();
        
        $start_date = date("Y-m-d");
        $start_date = date("2019-04-11");
        $rep_arr_tmp = array();
        foreach($site_col[$site_id] AS $col => $val){
            $rep_arr_tmp[] .= " ".$col." AS ".$col." ";
        }
        $sql_str = implode(",",$rep_arr_tmp);
        $sql_str .= ", kuchikomi_cnt,";
        $sql_str .= " keyword_mst_seq";
        
        
        $query = DB::connection('reputation')->table("rep_".$site_id."_reptation");
        $query->selectRaw(
                $sql_str
                ); 
        $query->where('date',$start_date);
        $query->where('kuchikomi_cnt',">=",1);
        
        $data = $query->orderByRaw('total DESC,kuchikomi_cnt DESC ')->get();
        $data = json_decode(json_encode($data), true);
        
        static::$allCnt = count($data);
        
        
        $site_hotel_arr = array();
        foreach($data AS $site_data){
            $site_hotel_arr[] = $site_data['keyword_mst_seq'];
        }
        
        if(count($site_hotel_arr) == 0){
            return $data;
        }
        
        // ホテル名取得
        $s_data_h = self::getSiteHotelName($site_hotel_arr,$site_id);
        
        // if($site_id == 'ikyu' || $site_id == 'ikyubiz' ){
            // $acc_data = self::getIkyuAccCode($site_hotel_arr);
        // }
        static::$sel_keyword_mst_seq = array();
        $sel_keyword_mst_seq_cnt = count(static::$sel_keyword_mst_seq);
        $r_site_hotel_cd_cnt = count($r_site_hotel_cd);
        $sel_site_hotel_cd_cnt = count($sel_site_hotel_cd);
        
        $t_site_hotel_cd_cnt = count($t_site_hotel_cd);
        $all_site_hotel_cd_cnt = count($all_site_hotel_cd);
        
        $sel_keyword_mst_seq_flip = array_flip(static::$sel_keyword_mst_seq);
        
        $r_site_hotel_cd_flip = array_flip($r_site_hotel_cd);
        $sel_site_hotel_cd_flip = array_flip($sel_site_hotel_cd);
        $t_site_hotel_cd_flip = array_flip($t_site_hotel_cd);
        $all_site_hotel_cd_flip = array_flip($all_site_hotel_cd);
        
        $cnt = 0;
        $data_tmp = $data;
        $data_tmp2 = $data;
        unset($data);
        $data = array();
        $set_flg = true;;
        
        
        foreach($data_tmp AS $k => $site_data){
            
            // サイトのホテルのカラム名
            //$site_hotel_column = $site_hotel_col[$site_id];
            // サイトのホテル番号
            //$site_hotel_cd =  $site_data[$site_hotel_column];
            $data_tmp2[$k]['keyword'] = isset($s_data_h[$site_data['keyword_mst_seq']]['keyword'])?$s_data_h[$site_data['keyword_mst_seq']]['keyword']:'';
            $data_tmp2[$k]['keyword_mst_seq'] = isset($site_data['keyword_mst_seq'])?$site_data['keyword_mst_seq']:'';
            
            if(array_key_exists('total', $site_data)){
                if($site_data['total']==0){
                    continue;
                }
            }
            
            // if($all_site_hotel_cd_cnt != 0){
                // if(!isset($all_site_hotel_cd_flip[$site_hotel_cd])){
//                     
                    // continue;
                // }
            // }
//             
            // if($all_site_hotel_cd_cnt != 0){
                // if(!isset($all_site_hotel_cd_flip[$site_hotel_cd])){
//                     
                    // continue;
                // }
            // }
//             
            // if($r_site_hotel_cd_cnt != 0){
                // if(!isset($r_site_hotel_cd_flip[$site_hotel_cd])){
//                     
                    // continue;
                // }
            // }
            // if($set_flg){
                // // エリア選択
                // if($large_area_code != '' && $t_site_hotel_cd_cnt != 0){
                    // if(!isset($t_site_hotel_cd_flip[$site_hotel_cd])){
//                         
                        // continue;
                    // }
                // }
            // }
            
            $data[$cnt] = $site_data;
            
            $data[$cnt]['keyword'] = isset($s_data_h[$site_data['keyword_mst_seq']]['keyword'])?$s_data_h[$site_data['keyword_mst_seq']]['keyword']:'';
            $data[$cnt]['keyword_mst_seq'] = isset($s_data_h[$site_data['keyword_mst_seq']]['keyword_mst_seq'])?$s_data_h[$site_data['keyword_mst_seq']]['keyword_mst_seq']:'';
            // 施設情報更新チェック
            $data[$cnt]['last_update'] = isset($s_data_h[$site_data['keyword_mst_seq']]['last_update'])?$s_data_h[$site_data['keyword_mst_seq']]['last_update']:'';
            $data[$cnt]['area_region'] = isset($s_data_h[$site_data['keyword_mst_seq']]['area_region'])?$s_data_h[$site_data['keyword_mst_seq']]['area_region']:'';
            $data[$cnt]['area_prefecture'] = isset($s_data_h[$site_data['keyword_mst_seq']]['area_prefecture'])?$s_data_h[$site_data['keyword_mst_seq']]['area_prefecture']:'';
            $data[$cnt]['area_large_area'] = isset($s_data_h[$site_data['keyword_mst_seq']]['area_large_area'])?$s_data_h[$site_data['keyword_mst_seq']]['area_large_area']:'';
            
            $data[$cnt]['rank'] = $cnt +0*100+1;
            $cnt++;
        }

        return $data;
    }

    //ruikei取得
    public static function getSiteRepOneDay($site_id,$keyword_mst_seq,$today_date){
        
        $query = DB::connection('reputation')->table("rep_".$site_id."_reptation");
        $query->where('keyword_mst_seq',$keyword_mst_seq);
        $query->where('date',$today_date); 
        
        $rep = $query->first();
        $rep = json_decode(json_encode($rep), true);
        
        return $rep;
    }
    
    public static function getKuchiRepAvgRow($site_id,$keyword_mst_seqs,$start_datetime,$end_datetime){
        $hotel_col = self::getHotelCol();
        $kuchi_col_name_list = self::getKuchiColName();
        $site_arr = array_keys($kuchi_col_name_list[$site_id]);
        
        $rep_arr_tmp = array();
        foreach($site_arr AS $col){
            if($site_id == "rakuten" && $col == "food"){
                $rep_arr_tmp[] .= "ROUND(AVG(IF (meal_score !=0, meal_score ,null)),2) AS ".$col." "; 
            }elseif(($site_id == "rurubu" && $col == "food") 
            || ($site_id == "tripadvisor" && $col == "bedroom")){
                $rep_arr_tmp[] .= "ROUND(AVG(IF (".$col." !=0,".$col." ,null)),2) AS ".$col." ";
            }else if($site_id == "expedia" && $col == "conditions"){
                $rep_arr_tmp[] .= "ROUND(AVG(IF (condition_score !=0, condition_score ,null)),2) AS ".$col." "; 
            }else{
                $rep_arr_tmp[] .= "ROUND(AVG(IF (".$col."_score !=0,".$col."_score ,null)),2) AS ".$col." ";
            }
        }
        
        $sql_str = implode(",",$rep_arr_tmp);
        $sql_str .= " , ps_hotel_id as keyword_mst_seq ,";
        $sql_str .= " count("."ps_hotel_id".") AS kuchikomi_cnt ";
        
        $query = DB::connection('review')->table($site_id.'_kuchikomi');
        $query->selectRaw(
                    $sql_str
                    ); 
        $query->where('post_date',">=",$start_datetime);
        $query->where('post_date',"<=",$end_datetime);
        $query->where('ps_hotel_id',$keyword_mst_seqs);
        
        $row = $query->first();
        $row = json_decode(json_encode($row), true);

        return $row;
    }
    
    public static function getLargeAreaCode($keyword_mst_seq){
        
        $query = DB::connection('reviewM')->table('hotel_details');
        $query->select(
                    'area_large_area_cd'
                    );
        $query->where('keyword_mst_seq',$keyword_mst_seq);
        $res = $query->pluck('area_large_area_cd');
        $res = json_decode(json_encode($res), true);
        
        $row = "";
        if(isset($res[0])){
                $row = $res[0];    
        }
        
        return $row;
    }
    
    public static function  getReptationTotal(){
        $arr = array(           'rakuten'=>array('total'=>'総合'),
                                'jalan'=>array('total'=>'総合'),
                                'rurubu'=>array('total'=>'総合'),
                                'tripadvisor'=>array('total'=>'総合'),
                                'expedia'=>array('total'=>'総合'),
                                'booking'=>array('total'=>'総合'),
                                'agoda'=>array('total'=>'総合'),
                                );
        return $arr;
    }
    
    public static function  getBrandHotelCntData(){
        
        $query = DB::connection('reviewM')->table('hotel_brand AS B')
                    ->crossJoin('hotel_details as H');
        $query->select(DB::raw('B.seq,COUNT(B.seq) AS hotel_cnt')); 
        $query->whereRaw('(B.seq = H.hotel_chain_1 OR B.seq = H.hotel_chain_2 OR B.seq = H.hotel_chain_3)'); 
        $query->where([
                        ['B.delete_flg', '=', '0']
                ]);       
        $dataTmp = $query->groupBy('B.seq')->get();
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        $data = array();
        if(count($dataTmp) >0 ){
            foreach ($dataTmp as $k => $v) {
                $data[$v['seq']] = $v['hotel_cnt'];
            }    
        }
        
        return $data;
    }
    
    public static function  getChainHotelCntData(){
        
        $query = DB::connection('reviewM')->table('hotel_chain AS C')
                    ->crossJoin('hotel_details as H');
        $query->select(DB::raw('C.seq,COUNT(C.seq) AS hotel_cnt')); 
        $query->whereRaw('(C.seq = H.hotel_chain_1 OR C.seq = H.hotel_chain_2 OR C.seq = H.hotel_chain_3)'); 
        $query->where([
                        ['C.delete_flg', '=', '0']
                ]);       
        $dataTmp = $query->groupBy('C.seq')->get();
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        $data = array();
        if(count($dataTmp) >0 ){
            foreach ($dataTmp as $k => $v) {
                $data[$v['seq']] = $v['hotel_cnt'];
            }    
        }
        
        return $data;
    }
    
    public static function  getBrandHotelKeywordMstSeq(){
        $query = DB::connection('reviewM')->table('hotel_brand AS B')
                    ->crossJoin('hotel_details as H');
        $query->select(DB::raw('H.keyword_mst_seq')); 
        $query->whereRaw("B.delete_flg = '0' AND (H.hotel_brand_1=B.seq OR H.hotel_brand_2=B.seq OR H.hotel_brand_3=B.seq)"); 
              
        $dataTmp = $query->pluck('H.keyword_mst_seq');
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        return $dataTmp;
        
    }
    
    public static function  getBrandHotelSiteCode($site_id,$col){
        
        $query = DB::connection('reviewM')->table('hotel_brand AS B')
                    ->crossJoin('hotel_details as H')
                    ->crossJoin("rep_".$site_id."_mst AS S");
        $query->select(DB::raw("S.".$col)); 
        $query->whereRaw("B.delete_flg = '0' AND S.repchecker_keyword_mst_seq = H.keyword_mst_seq AND (H.hotel_brand_1=B.seq OR H.hotel_brand_2=B.seq OR H.hotel_brand_3=B.seq)"); 
              
        $dataTmp = $query->pluck("S.".$col);
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        return $dataTmp;
        
        // $sql = "SELECT S.".$col." FROM rep_".$site_id."_mst AS S , hotel_details AS H , hotel_brand AS B  ";
        // $sql .= "WHERE B.delete_flg = '0' AND S.repchecker_keyword_mst_seq = H.keyword_mst_seq AND (H.hotel_brand_1=B.seq OR H.hotel_brand_2=B.seq OR H.hotel_brand_3=B.seq) ";
//         
        // $data = SQL::selData(self::_lib,'repchecker',$sql,'Col',__FILE__,__FUNCTION__,__LINE__);
        // return $data;
    }

    public static function  getTokyustayChainList(){
            
        $sql_str =  " hotel_details.keyword_mst_seq, ";
        $sql_str .=  " hotel_chain.name AS group_name, ";
        $sql_str .=  " hotel_chain.seq AS group_seq ";
        
        $query = DB::connection('reviewM')->table('hotel_chain')
                    ->leftJoin('hotel_details', 'hotel_details.hotel_chain_1', '=', 'hotel_chain.seq');
        $query->selectRaw(
                $sql_str
                ); 
        
        $query->whereRaw("hotel_details.hotel_chain_1 = 93"); 
              
        $dataTmp = $query->get();
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        $data = array();
        foreach ($dataTmp as $k => $v) {
            $data[$v['keyword_mst_seq']] = $v;
        }
        
        return $data;
        
    }

    public static function  getChainHotelKeywordMstSeq(){
            
        $query = DB::connection('reviewM')->table('hotel_brand AS C')
                    ->crossJoin('hotel_details as H');
        $query->select(DB::raw('H.keyword_mst_seq')); 
        $query->whereRaw("C.delete_flg = '0' AND hotel_chain=C.seq"); 
              
        $dataTmp = $query->pluck('H.keyword_mst_seq');
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        return $dataTmp;
        
    }
    
    public static function  getChainHotelSiteCode($site_id,$col){
            
        $query = DB::connection('reviewM')->table('hotel_brand AS C')
                    ->crossJoin('hotel_details as H')
                    ->crossJoin("rep_".$site_id."_mst AS S");
        $query->select(DB::raw("S.".$col)); 
        $query->whereRaw("C.delete_flg = '0' AND S.repchecker_keyword_mst_seq = H.keyword_mst_seq AND (H.hotel_chain_1=C.seq OR H.hotel_chain_2=C.seq OR H.hotel_chain_3=C.seq)"); 
              
        $dataTmp = $query->pluck("S.".$col);
        $dataTmp = json_decode(json_encode($dataTmp), true);
        
        return $dataTmp; 
            
        // $sql = "SELECT S.".$col." FROM rep_".$site_id."_mst AS S , hotel_details AS H , hotel_chain AS C  ";
        // $sql .= "WHERE C.delete_flg = '0' AND S.repchecker_keyword_mst_seq = H.keyword_mst_seq AND (H.hotel_chain_1=C.seq OR H.hotel_chain_2=C.seq OR H.hotel_chain_3=C.seq) ";
//         
        // $data = SQL::selData(self::_lib,'repchecker',$sql,'Col',__FILE__,__FUNCTION__,__LINE__);
//         
        // return $data;
    }
    
    public static function  getSiteHotelName($data,$site_id){
        
        $sql_str =  " hotel_name as keyword, ";
        $sql_str .=  " area_region, ";
        $sql_str .=  " area_prefecture, ";
        $sql_str .=  " area_large_area, ";
        $sql_str .=  " keyword_mst_seq, ";
        $sql_str .=  " last_update ";
        
        $query = DB::connection('reviewM')->table('hotel_details');
        $query->selectRaw(
                $sql_str
                ); 
        
        $query->whereIn("keyword_mst_seq",$data); 
              
        $data_tmp = $query->get();
        $data_tmp = json_decode(json_encode($data_tmp), true);
        
        $data = array();
        foreach($data_tmp AS $k => $data_arr){
            $data[$data_arr['keyword_mst_seq']] = $data_arr;
        }
        return $data;
    }
    
    public static function  getKuchiColName(){
        $arr = array(
                            'rakuten'=>array('total'=>'総合','service'=>'サービス','location'=>'立地','room'=>'部屋','amenity'=>'設備・アメニティ','bathroom'=>'風呂','food'=>'食事',),
                            'jalan'=>array('total'=>'総合','room'=>'部屋','bathroom'=>'風呂','breakfast'=>'料理（朝食）','dinner'=>'料理（夕食）','service'=>'接客・サービス','clean'=>'清潔感',),
                            'rurubu'=>array('total'=>'総合','room'=>'部屋','service'=>'サービス','food'=>'食事','location'=>'立地','amenity'=>'設備','bathroom'=>'風呂',),
                            'tripadvisor'=>array('total'=>'総合','location'=>'立地','bedroom'=>'寝心地','room'=>'客室','service'=>'サービス','price'=>'価格','clean'=>'清潔感',),
                            'expedia'=>array('total'=>'総合','clean'=>'清潔感','service'=>'サービス','room'=>'客室','conditions'=>'設備'),
                            'booking'=>array('total'=>'総合',),
                            'agoda'=>array('total'=>'総合',),
                    );
        return $arr;
    }
    
    public static function getDictionarySeq($company_seq,$keyword_mst_seq){
        
        $query = DB::connection('tokyustay')->table('vctm_vc_mst_tokyustay');
        $query->select(
                    'seq'
                    );
        $query->where([
                        ['status', '=', '1'],
                        ['company_seq', '=', $company_seq],
                        ['default_word_list_seq', '=', '74']
                ]);
        $query->where('account_keyword_mst_seq', $keyword_mst_seq);
        
        $res = $query->pluck('seq');
        $res = json_decode(json_encode($res), true);
        
        return $res;
    }
    
    public static function getTargetVcMstSeqs(){
        
        $query = DB::connection('tokyustay')->table('vctm_vc_mst_tokyustay');
        $query->select(
                    'seq',
                    'company_seq',
                    'account_keyword_mst_seq'
                    );
        $query->where([
                        ['status', '=', '1'],
                        ['company_seq', '=', static::$customer_company_seq],
                        ['default_word_list_seq', '=', '74']
                ]);
        
        $res = $query->get();
        $res = json_decode(json_encode($res), true);
        
        return $res;
    }
    
    public static function getVcAnalyzeResult($company_seq,$vc_mst_seqs,$date_from,$date_to){
        //$to = $now = date("Y-m-d H:i:s");
        $from = date("Y-m-d H:i:s",strtotime($date_from));
        $to = date("Y-m-d H:i:s",strtotime($date_to));
        
        $table = "vctm_vc_analyze_result_4633";
        
        $query = DB::connection('tokyustay')->table($table);
        $query->select(
                    'mng_kuchikomi_mst_seq',
                    'table_name',
                    'company_seq',
                    'site_id',
                    'post_date',
                    'vc_kind',
                    'vc_kw',
                    'age',
                    'sex',
                    'vc_mst_seq',
                    'purpose',
                    'sentence_cnt',
                    'sentence',
                    'cnt',
                    'survey_seq',
                    'survey_answer_base_seq',
                    'survey_answer_trn_seq'
                    );            
        $query->where([
                        ['company_seq', '=', '4633'],
                        ['vc_kind', '=', 'naga_posi_list']
                ]);
        $query->whereRaw("lang IN ('','ja')");
        $query->whereIn('vc_mst_seq',$vc_mst_seqs);
        $query->where('post_date', '>=' ,$from);
        $query->where('post_date', '<=' ,$to);
        
        $res = $query->orderByRaw('post_date DESC')->get()->toarray();
        $res = json_decode(json_encode($res), true);
        
        return $res;
    }

    public static function  getAccountKeywordsAll($company_seq, $account_keyword_mst_seq, $vc_mst_seq){
        
        $query = DB::connection('tokyustay')->table('vctm_activity_mst_tokyustay');
        $query->select('vc_act_set');
        $query->where([
                        ['company_seq', '=', '4633'],
                        ['account_keyword_mst_seq', '=', $account_keyword_mst_seq],
                        
                ]);
        $query->whereIn('vc_mst_seq',$vc_mst_seq);
        
        $vc_act_set = $query->pluck('vc_act_set')->first();
        $vc_act_set = json_decode(json_encode($vc_act_set), true);
        
        if ($vc_act_set == false) {
            return array();
        }
        
        
        $vc_act_set_arr = array_merge(array(), explode(",", $vc_act_set));
        
        if (count($vc_act_set_arr) == 0) {
            return $vc_act_set_arr;
        }
        for ($i = 0; $i < count($vc_act_set_arr);  ++$i) {
            $vc_act_set_arr[$i] = $vc_act_set_arr[$i];
        }
        
        
        $sql_str = " ACT.seq AS act_seq,ACT.act_name, ";
        $sql_str .= " CMP.seq AS comp_seq,CMP.comp_name, ";
        $sql_str .= " CMKW.seq AS comp_kwd_seq,CMKW.comp_kwd_name ";
        
        $query = DB::connection('tokyustay')->table('vctm_account_vc_activity_list_tokyustay AS ACT')
                ->leftJoin('vctm_account_vc_component_list_tokyustay AS CMP', 'ACT.seq', '=', 'CMP.account_vc_activity_list_seq')
                ->leftJoin('vctm_account_vc_component_keyword_list_tokyustay AS CMKW','ACT.seq', '=', 'CMKW.account_vc_activity_list_seq');
        
        $query->selectRaw(
                    $sql_str
                    ); 
        $query->where([
                        ['ACT.company_seq', '=', '4633'],
                        ['ACT.account_keyword_mst_seq', '=', $account_keyword_mst_seq],
                        ['ACT.disp_flg', '=', 'on'],
                        ['CMP.company_seq', '=', '4633'],
                        ['CMP.account_keyword_mst_seq', '=', $account_keyword_mst_seq],
                        ['CMP.disp_flg', '=', 'on'],
                        ['CMKW.company_seq', '=', '4633'],
                        ['CMKW.account_keyword_mst_seq', '=', $account_keyword_mst_seq],
                        ['CMKW.disp_flg', '=', 'on'],
                        
                ]);
        
        $query->whereIn('ACT.vc_mst_seq',$vc_mst_seq);
        $query->whereIn('CMP.vc_mst_seq',$vc_mst_seq);
        $query->whereIn('CMKW.vc_mst_seq',$vc_mst_seq);
        $query->whereIn('ACT.seq',$vc_act_set_arr);
        
        $res = $query->get();
        $res = json_decode(json_encode($res), true);
         
        //$sql = " SELECT ";
        //$sql .= " ACT.seq AS act_seq,ACT.act_name ";
        //$sql .= " ,CMP.seq AS comp_seq,CMP.comp_name ";
        //$sql .= " ,CMKW.seq AS comp_kwd_seq,CMKW.comp_kwd_name ";
        //$sql .= " FROM vctm_account_vc_activity_list AS ACT ";
        //$sql .= " LEFT JOIN vctm_account_vc_component_list AS CMP ON (ACT.seq = CMP.account_vc_activity_list_seq) ";
        //$sql .= " LEFT JOIN vctm_account_vc_component_keyword_list AS CMKW ON (ACT.seq = CMKW.account_vc_activity_list_seq AND CMP.seq = CMKW.account_vc_component_list_seq) ";
        //$sql .= " WHERE  ";
        //$sql .= " ACT.company_seq = " . SQL::quote($company_seq) . " AND ACT.account_keyword_mst_seq = " . SQL::quote($account_keyword_mst_seq) . " AND ACT.vc_mst_seq IN (" . implode(",", $vc_mst_seq) . ") ";
        //$sql .= " AND ACT.disp_flg = 'on' AND ACT.seq IN (" . implode(",", $vc_act_set_arr) . ") ";
        //$sql .= " AND CMP.company_seq = " . SQL::quote($company_seq) . " AND CMP.account_keyword_mst_seq = " . SQL::quote($account_keyword_mst_seq) . " AND CMP.vc_mst_seq IN (" . implode(",", $vc_mst_seq) . ") ";
        //$sql .= " AND CMP.disp_flg = 'on' ";
        //$sql .= " AND CMKW.company_seq = " . SQL::quote($company_seq) . " AND CMKW.account_keyword_mst_seq = " . SQL::quote($account_keyword_mst_seq) . " AND CMKW.vc_mst_seq IN (" . implode(",", $vc_mst_seq) . ") ";
        //$sql .= " AND CMKW.disp_flg = 'on' ";
        // try {
            // //echo "<!-- ";var_dump($sql); echo " -->";
            // $res = SQL::selData(self::_lib,'repchecker',$sql,'All',__FILE__,__FUNCTION__,__LINE__);
        // } catch (Exception $e) {
            // //self::lib->dbErr($e, "\nSQL：" . $sql . "\nfile：" . __FILE__ . "\nLine：" . __LINE__ . "\n");
            // exit;
        // }
        return $res;
    }

    public static function getPairMngKuchikomiSeqNkuchikomiSeq($site_id,$mng_kuchikomi_seq_list){
        
        $table = "mng_kuchikomi_mst";
        $sql_str = " seq as mng_kuchikomi_seq, kuchikomi_seq ";
        $query = DB::connection('review')->table($table);
        $query->selectRaw(
            $sql_str
            );            
        $query->where([
                        ['site_id', '=', $site_id],
                       
                ]);
        $query->whereIn('seq',$mng_kuchikomi_seq_list);

        $res = $query->groupBy('mng_kuchikomi_seq')->get()->toarray();
        $res = json_decode(json_encode($res), true);
         
        return $res;
    }
    
    public static function getGroupingDataAll($company_seq,$keyword_mst_seq,$vc_mst_seqs){
        
        $table = "vctm_account_vc_nega_posi_list_tokyustay";
        $sql_str = " dict_name,grouping ";
        $query = DB::connection('tokyustay')->table($table);
        $query->selectRaw(
            $sql_str
            );            
        $query->where([
                        ['company_seq', '=', '4633'],
                        ['account_keyword_mst_seq', '=', $keyword_mst_seq],
                        ['disp_flg', '=', 'on'],
                ]);
        $query->whereIn('vc_mst_seq',$vc_mst_seqs);

        $resTmp = $query->get()->toarray();
        $resTmp = json_decode(json_encode($resTmp), true);
        
        $res = array();
        foreach ($resTmp as $k => $v) {
            $res[$v['dict_name']] = $v['grouping'];
        }
        
        return $res;
        
        //$sql = " SELECT dict_name,grouping ";
        //$sql .= " FROM vctm_account_vc_nega_posi_list ";
        //$sql .= " WHERE company_seq = ".SQL::quote($company_seq)." "; 
        //$sql .= " AND account_keyword_mst_seq = ".SQL::quote($keyword_mst_seq)." ";
        //$sql .= " AND vc_mst_seq IN (".implode(",",$vc_mst_seqs).") ";
        //$sql .= " AND disp_flg ='on' ";
        //$res = self::db->fetchPairs($sql);
        //return $res = SQL::selData(self::_lib,'repchecker',$sql,'Pairs',__FILE__,__FUNCTION__,__LINE__);
        
        //return $res;
    }
    public static function DeleteValuechainAnalyzeTarget($keyword_mst_seq, $base_set_date){
        
        $sql = "DELETE FROM vctm_vc_analyze_target_4633 WHERE company_seq = '4633' AND keyword_mst_seq = '".$keyword_mst_seq."' AND post_date < '".$base_set_date."'";
        
        DB::connection('tokyustay')->statement($sql);
        
    }
    
    public static function getAnalyzeTarget($info){
        
        $table = "vctm_vc_analyze_target_4633";
        
        $query = DB::connection('tokyustay')->table($table);
        $sql_str = " CONCAT(site_id,'-',kuchikomi_seq) as site_seq ";
        $query->selectRaw(
                $sql_str
                );            
        $query->where([
                        ['company_seq', '=', '4633'],
                        ['keyword_mst_seq', '=', $info['account_keyword_mst_seq']],
                       
                ]);
        
        $list = $query->pluck('site_seq');
        $list = json_decode(json_encode($list), true);
        
        return $list;
        
    }
    
    public static function insAnalyzeTarget($info,$insert,$unique_id){
        
        $table = "vctm_vc_analyze_target_4633";
        
        $sqls = array();
        $tmp = array();
        $num = 0;
        for ($j=0, $max_j=count($insert);$j<$max_j;++$j) {
            foreach ($insert[$j] as $key => $val) {
                $insert[$j][$key] = "'".$val."'";
            }
            $sqls[$num][] = '('.implode(',', $insert[$j]).')';
            if ($j != 0 && $j % 20 == 0) {
                ++$num;
            }
        }
        
        for ($j=0, $max_j=count($sqls);$j<$max_j;++$j) {
            $sql = " REPLACE INTO ".$table." (seq,site_id,kuchikomi_seq,keyword_mst_seq,lang,post_date,comment,get_url,get_date,add_info,manual_cus_seq,delete_flg,company_seq) VALUES  ";
            $sql .= join(",", $sqls[$j]).";";
            
            try {
                DB::connection('tokyustay')->statement($sql);
            } catch (Exception $e) {
                
            }
            
        }
        
    }
    
    public static function getTargetMngkuchikomimst($info,$base_set_date , $unique_id){
        
        $table = "mng_kuchikomi_mst";
        
        $query = DB::connection('review')->table($table);
        $sql_str = " seq,site_id,kuchikomi_seq,keyword_mst_seq,lang,post_date, ";
        //$sql_str .= " IF( comment LIKE '%--PSINC%' , CONCAT(REPLACE(comment,SUBSTRING(comment,LOCATE('--PSINC',comment)),''),'\n') , CONCAT(comment,'\n\n') ) AS comment, ";
        $sql_str .= " IF( comment LIKE '%--PSINC%' , CONCAT(REPLACE(comment,SUBSTRING(comment,LOCATE('--PSINC',comment)),''),'') , CONCAT(comment,'') ) AS comment, ";
        $sql_str .= " get_url,get_date,add_info,manual_cus_seq,delete_flg, 4633 ";
        
        $query->selectRaw(
                $sql_str
                );            
        $query->where([
                        ['site_id', '!=', 'twitter'],
                        ['site_id', '!=', 'google'],
                        ['post_date', '>=', $base_set_date],
                        ['keyword_mst_seq', '=', $info['account_keyword_mst_seq']],
                       
                ]);
                
        if (count($unique_id) != 0) {
            for ($i=0, $max_i=count($unique_id);$i<$max_i;++$i) {
                $unique_id[$i] = "'".$unique_id[$i]."'";
            }
            
            $query->whereRaw("CONCAT(site_id,'-',kuchikomi_seq) NOT IN (".  join(',', $unique_id).")  ");
        }
        $list = $query->get();
        $list = json_decode(json_encode($list), true);
        
        return $list;
        
    }
        
    public static function ValuechainStart(){
            
        //target_select_insert
        $list = self::getTargetVcMstSeqs();
        
        $base_set_date = date("Y-m-d 00:00:00",strtotime("-24 month"));
        
        foreach ($list as $k => $v) {
                
            echo "start : ".$v['account_keyword_mst_seq']." \n";
            
            self::DeleteValuechainAnalyzeTarget($v['account_keyword_mst_seq'],$base_set_date);
            
            $unique_id = self::getAnalyzeTarget($v);
            
            $insert = self::getTargetMngkuchikomimst($v,$base_set_date , $unique_id);
            
            self::insAnalyzeTarget($v,$insert,$unique_id);
        
        }
        
        
        dd($list);
        
        
        // /usr/bin/php ${absdir}/delete_table_data.php $1 $2
        #(この処理は1台目のサーバーで行う)

        #全件の起動
        // /usr/bin/php ${absdir}/start_all_analyze.php $1 $2
    }
    
    //1.クチコミ評価分析（評価点数）
    public static function TokyuStayReport(Request $request){
            
        $data = $request::all();
        
        ini_set("memory_limit", "-1");
        ini_set("max_execution_time",0); // タイムアウトしない
        ini_set("max_input_time",0); // パース時間を設定しない
        
                    
        if(isset($data['start_date'])){
            $date_from = $data['start_date'];
        }else{
            $date_from = date("Y-m-d");
        }
        
        if(isset($data['end_date'])){
            $date_to = $data['end_date'];
        }else{
            $date_to = date("Y-m-d");
        }
        
        if(isset($data['hotel_id'])){
            $keyword_mst_seq = $data['hotel_id'];
        }else{
            $keyword_mst_seq = '61635';
        }    
        
        $excel = new ExcelReviser;
        $excel->setInternalCharset('utf-8');
        
        $site_info = self::getRankingSites();
        $sex_type = self::getSex();
        
        $today_date = date("Y-m-d");
        //１．属性（単位　：　室　）
        //表示しない
        
        //2. OTA別評価点:ホームページ画面表示  
        $rep_arr = self::getReptation();
        
        $large_area_code = self::getLargeAreaCode($keyword_mst_seq);
        $hotel_name = self::getHotelName($keyword_mst_seq);
        
        //2-1 東急ステイ平均
        $ruikei_tokyustay_avg_tmp = array();
        //2-2 エリア平均
        //2-2-1 エリア平均総合
        $area = array();
        //2-2-2 エリア順位
        $ruikei_area_avg_tmp = array();
        //2-3 ＯＴＡ別評価点 現在
        $ruikei_point_tmp = array();
        //2-4 ＯＴＡ別評価点 前回
        $ruikei_before_point_tmp = array();
       
        //3. 口コミ分析
        $date_from_before_3month = date("Y-m-d", strtotime($date_from. "-3 months"));
        $date_to_before_3month = date("Y-m-d", strtotime($date_to. "-3 months"));
        $today_before_3month = date("Y-m-d", strtotime($today_date. "-3 months"));
        //3-1 口コミ分析 現在
        $kuchikomi_avg_point_tmp = array();
        //3-2 口コミ分析 前回
        $kuchikomi_before_avg_point_tmp = array();
        
        //4. 項目別コメント
        foreach ($rep_arr as $site_id => $value) {
              
            //2. OTA別評価点:ホームページ画面表示
            $t_site_hotel_cd = self::getAreaSiteHotelCode($sel_keymst_seq = array(),$site_id,$large_area_code);
            $keyword_mst_seqs = self::getAreaKeywordSeqs($sel_keymst_seq = array(),$site_id,$large_area_code);
            $sel_site_hotel_cd = self::getSelectSiteHotelCode($site_id);
            $all_site_hotel_cd = self::getAllSiteHotelCode($site_id);
            
            
            //2-1 東急ステイ平均
            $ruikei_tokyustay_avg_tmp[$site_id] = self::getReptationRuikeiGroupData('chain',$site_id,'',$t_site_hotel_cd,$sel_site_hotel_cd,$all_site_hotel_cd);
            
            //2-2 エリア平均
            //2-2-1 エリア平均総合
            $area[$site_id] = self::getReptationRuikeiAllData('row',$site_id,$keyword_mst_seqs,$large_area_code);
            
            
            //2-2-2 エリア順位
            $ruikei_area_avg_tmp[$site_id] = self::getReptationRuikeiData($site_id,'',$t_site_hotel_cd,$sel_site_hotel_cd,$all_site_hotel_cd,array(),$large_area_code);
                                                    
            //2-3 ＯＴＡ別評価点 現在
            $ruikei_point_tmp[$site_id] =  self::getSiteRepOneDay($site_id,$keyword_mst_seq,$today_date);
            
            //3-1 口コミ分析 現在
            $kuchikomi_avg_point_tmp[$site_id] =  self::getKuchiRepAvgRow($site_id,$keyword_mst_seq,$date_from,$date_to);
            
            //3-2 口コミ分析 前回
            $kuchikomi_before_avg_point_tmp[$site_id] =  self::getKuchiRepAvgRow($site_id,$keyword_mst_seq,$date_from_before_3month,$date_to_before_3month);
            
        }
        
        //2-2-2 エリア順位
        $ruikei_area_avg = array();
        foreach ($ruikei_area_avg_tmp as $site_id => $val) {
            foreach ($val as $k => $v) {
                $ruikei_area_avg[$site_id][$v['keyword_mst_seq']] =$v;
            }
        }
        
        
        $sheet_count = 0;
        $excel->addString($sheet_count,4,1,"店名 : ".$hotel_name,0,8,1);
        $excel->addString($sheet_count,1,9,date('Y年m月d日'));
        $excel->addString($sheet_count,24,2,date('Y年m月d日',strtotime($date_from))."～".date('Y年m月d日',strtotime($date_to)),0,6,1);
        
        //2-1 東急ステイ平均
        $ruikei_tokyustay_avg_tmp_col =4;
        //2-2-1 エリア平均総合
        $area_col = 4;
        //2-2-2 エリア順位
        $ruikei_area_avg_tmp_col = 4;
        //2-3 ＯＴＡ別評価点 現在
        $ruikei_point_tmp_col =4;
        //2-4 ＯＴＡ別評価点 前回 total
        $ruikei_before_point_tmp_col =4;
        
        //3-1 口コミ分析 現在
        $kuchikomi_avg_point_tmp_col = 4;
        //3-2 口コミ分析 前回
        $kuchikomi_before_avg_point_tmp_col = 4;
        
        //抽出月:
        foreach ($rep_arr as $site_id => $value) {
            //2-1 東急ステイ平均
            if($site_id == "booking" ||$site_id == "agoda"){
                if(isset($ruikei_tokyustay_avg_tmp[$site_id]['total']) 
                || (isset($ruikei_tokyustay_avg_tmp[$site_id]['total']) && $ruikei_tokyustay_avg_tmp[$site_id]['total']/2 != 0)){
                    $excel->addNumber($sheet_count,9,$ruikei_tokyustay_avg_tmp_col++,round($ruikei_tokyustay_avg_tmp[$site_id]['total']/2,2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,9,$ruikei_tokyustay_avg_tmp_col++,"",0,0,1);
                }
                
            }else{
                if(isset($ruikei_tokyustay_avg_tmp[$site_id]['total']) 
                || (isset($ruikei_tokyustay_avg_tmp[$site_id]['total']) && $ruikei_tokyustay_avg_tmp[$site_id]['total'] != 0)){
                  $excel->addNumber($sheet_count,9,$ruikei_tokyustay_avg_tmp_col++,round($ruikei_tokyustay_avg_tmp[$site_id]['total'],2),0,0,1);  
                }else{
                  //$excel->addString($sheet_count,9,$ruikei_tokyustay_avg_tmp_col++,$ruikei_tokyustay_avg_tmp[$site_id]['total'],0,0,1);  
                  $excel->addString($sheet_count,9,$ruikei_tokyustay_avg_tmp_col++,0,0,0,1);
                }
            }        
                    
            //2-2 エリア平均
            //2-2-1 エリア平均総合
            if($site_id == "booking" ||$site_id == "agoda"){
                if(isset($area[$site_id]['total']) 
                || (isset($area[$site_id]['total']) && $area[$site_id]['total'] != 0)){
                    $excel->addNumber($sheet_count,12,$area_col++,round($area[$site_id]['total']/2,2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,12,$area_col++,'-',0,0,1);
                }
                
            }else{
                if(isset($area[$site_id]['total']) 
                || (isset($area[$site_id]['total']) && $area[$site_id]['total'] != 0)){
                    $excel->addNumber($sheet_count,12,$area_col++,round($area[$site_id]['total'],2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,12,$area_col++,'-',0,0,1);
                }
            }
//             
            // //2-2-2 エリア順位
            if(isset($ruikei_area_avg[$site_id][$keyword_mst_seq]['rank'])){
                $excel->addNumber($sheet_count,13,$ruikei_area_avg_tmp_col++,$ruikei_area_avg[$site_id][$keyword_mst_seq]['rank'],0,0,1);   
            }else{
                $excel->addString($sheet_count,13,$ruikei_area_avg_tmp_col++,"-",0,0,1);
            }
             
//             
//             
            // //2-3 ＯＴＡ別評価点 現在 total
            if($site_id == "booking" ||$site_id == "agoda"){
                if(isset($ruikei_point_tmp[$site_id]['total']) 
                || (isset($ruikei_point_tmp[$site_id]['total']) && $ruikei_point_tmp[$site_id]['total'] != 0)){
                    $excel->addNumber($sheet_count,16,$ruikei_point_tmp_col++,round($ruikei_point_tmp[$site_id]['total']/2,2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,16,$ruikei_point_tmp_col++,"-",0,0,1);
                }
            }else{
                if(isset($ruikei_point_tmp[$site_id]['total']) 
                || (isset($ruikei_point_tmp[$site_id]['total']) && $ruikei_point_tmp[$site_id]['total'] != 0)){
                    $excel->addNumber($sheet_count,16,$ruikei_point_tmp_col++,round($ruikei_point_tmp[$site_id]['total'],2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,16,$ruikei_point_tmp_col++,"-",0,0,1);
                }
            }
            // //2-3 ＯＴＡ別評価点 現在 total
            if($site_id == "booking" ||$site_id == "agoda"){
                if(isset($kuchikomi_avg_point_tmp[$site_id]['total']) && round($kuchikomi_avg_point_tmp[$site_id]['total'],0) != 0){
                    $excel->addNumber($sheet_count,28,$kuchikomi_avg_point_tmp_col++,round($kuchikomi_avg_point_tmp[$site_id]['total']/2,2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,28,$kuchikomi_avg_point_tmp_col++,"-",0,0,1);
                }
                
            }else{
                if(isset($kuchikomi_avg_point_tmp[$site_id]['total']) && round($kuchikomi_avg_point_tmp[$site_id]['total'],0) != 0){
                    $excel->addNumber($sheet_count,28,$kuchikomi_avg_point_tmp_col++,round($kuchikomi_avg_point_tmp[$site_id]['total'],2),0,0,1);                    
                }else{
                    $excel->addString($sheet_count,28,$kuchikomi_avg_point_tmp_col++,"-",0,0,1);
                }
            }
//             
            // //2-4 ＯＴＡ別評価点 前回 total
            if($site_id == "booking" ||$site_id == "agoda"){
                if(isset($kuchikomi_before_avg_point_tmp[$site_id]['total']) && round($kuchikomi_before_avg_point_tmp[$site_id]['total'],0) != 0){
                    $excel->addNumber($sheet_count,34,$kuchikomi_before_avg_point_tmp_col++,round($kuchikomi_before_avg_point_tmp[$site_id]['total']/2,2),0,0,1);    
                }else{
                    $excel->addString($sheet_count,34,$kuchikomi_before_avg_point_tmp_col++,"-",0,0,1);
                }
                
            }else{
                if(isset($kuchikomi_before_avg_point_tmp[$site_id]['total']) && round($kuchikomi_before_avg_point_tmp[$site_id]['total'],2) !=0){
                    $excel->addNumber($sheet_count,34,$kuchikomi_before_avg_point_tmp_col++,$kuchikomi_before_avg_point_tmp[$site_id]['total'],0,0,1);    
                }else{
                    $excel->addString($sheet_count,34,$kuchikomi_before_avg_point_tmp_col++,"-",0,0,1);
                }
            }
        }
        
        /// order by
        // 1. 接客、サービス、スタッフ、スタッフ対応
        // 2. 朝食、食事、料理（朝食）、お食事
        
        //2-3 ＯＴＡ別評価点 現在
        //rakuten
        if(isset($ruikei_point_tmp['rakuten']) && count($ruikei_point_tmp['rakuten'])>0){
            if(isset($ruikei_point_tmp['rakuten']['searvice']) && round($ruikei_point_tmp['rakuten']['searvice'],0) != 0){
                $excel->addNumber($sheet_count,17,4,round($ruikei_point_tmp['rakuten']['searvice'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,17,4,"-",0,0,1);
            }    
            
            if(isset($ruikei_point_tmp['rakuten']['food']) && round($ruikei_point_tmp['rakuten']['food'],0) != 0){
                $excel->addNumber($sheet_count,18,4,round($ruikei_point_tmp['rakuten']['food'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,18,4,"-",0,0,1);
            }
            $excel->addString($sheet_count,19,4,"-",0,0,1);
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            $v3 = '';
            if($ruikei_point_tmp['rakuten']['room']!=0){
                $v1 = round($ruikei_point_tmp['rakuten']['room'],2);
                $cnt++;
            }
            
            if($ruikei_point_tmp['rakuten']['amenity']!=0){
                $v2 = round($ruikei_point_tmp['rakuten']['amenity'],2);
                $cnt++;
            }
            
            if($ruikei_point_tmp['rakuten']['bathroom']!=0){
                $v3 = round($ruikei_point_tmp['rakuten']['bathroom'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,20,4,round(($v1+$v2+$v3)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,20,4,"-",0,0,1);    
            }
           
           if(isset($ruikei_point_tmp['rakuten']['location'])  && round($ruikei_point_tmp['rakuten']['location'],0) != 0){
               $excel->addNumber($sheet_count,21,4,round($ruikei_point_tmp['rakuten']['location'],2),0,0,1);
           }else{
               $excel->addString($sheet_count,21,4,"-",0,0,1);
           }
        }
        // //jalan
        if(isset($ruikei_point_tmp['jalan']) && count($ruikei_point_tmp['jalan'])>0){
            
            if(isset($ruikei_point_tmp['jalan']['searvice']) && round($ruikei_point_tmp['jalan']['searvice'],0) != 0){
                $excel->addNumber($sheet_count,17,5,round($ruikei_point_tmp['jalan']['searvice'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,17,5,"-",0,0,1);   
            }
            if(isset($ruikei_point_tmp['jalan']['breakfast']) && round($ruikei_point_tmp['jalan']['breakfast'],0) !=0){
                $excel->addNumber($sheet_count,18,5,round($ruikei_point_tmp['jalan']['breakfast'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,18,5,"-",0,0,1);    
            }
            
            if(isset($ruikei_point_tmp['jalan']['clean']) && round($ruikei_point_tmp['jalan']['clean'],0) != 0){
                $excel->addNumber($sheet_count,19,5,round($ruikei_point_tmp['jalan']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,19,5,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            
            if($ruikei_point_tmp['jalan']['room']!=0){
                $v1 = round($ruikei_point_tmp['jalan']['room'],2);
                $cnt++;
            }
            if($ruikei_point_tmp['jalan']['bathroom']!=0){
                $v2 = round($ruikei_point_tmp['jalan']['bathroom'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,20,5,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,20,5,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,31,6,$ruikei_point_tmp['jalan']['room']!=0?round($ruikei_point_tmp['jalan']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,32,6,"",0,0,1);
            // $excel->addNumber($sheet_count,33,6,$ruikei_point_tmp['jalan']['bathroom']!=0?round($ruikei_point_tmp['jalan']['bathroom'],2):"",0,0,1);
            
            $excel->addString($sheet_count,21,5,"-",0,0,1);
        }
        // //rurubu
        if(isset($ruikei_point_tmp['rurubu']) && count($ruikei_point_tmp['rurubu'])>0){
            if(isset($ruikei_point_tmp['rurubu']['service']) && round($ruikei_point_tmp['rurubu']['service'],0) != 0){
                $excel->addNumber($sheet_count,17,6,round($ruikei_point_tmp['rurubu']['service'],2),0,0,1);
            }else{
                $excel->addString($sheet_count,17,6,"-",0,0,1);
            }
            if(isset($ruikei_point_tmp['rurubu']['food']) && round($ruikei_point_tmp['rurubu']['food'],0) != 0){
                $excel->addNumber($sheet_count,18,6,round($ruikei_point_tmp['rurubu']['food'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,18,6,"-",0,0,1);
            }
            $excel->addString($sheet_count,19,6,"-",0,0,1);
            
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            $v3 = '';
            if($ruikei_point_tmp['rurubu']['room']!=0){
                $v1 = round($ruikei_point_tmp['rurubu']['room'],2);
                $cnt++;
            }
            
            if($ruikei_point_tmp['rurubu']['amenity']!=0){
                $v2 = round($ruikei_point_tmp['rurubu']['amenity'],2);
                $cnt++;
            }
            
            if($ruikei_point_tmp['rurubu']['bathroom']!=0){
                $v3 = round($ruikei_point_tmp['rurubu']['bathroom'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,20,6,round(($v1+$v2+$v3)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,20,6,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,31,7,$ruikei_point_tmp['rurubu']['room']!=0?round($ruikei_point_tmp['rurubu']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,32,7,$ruikei_point_tmp['rurubu']['amenity']!=0?round($ruikei_point_tmp['rurubu']['amenity'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,7,$ruikei_point_tmp['rurubu']['bathroom']!=0?round($ruikei_point_tmp['rurubu']['bathroom'],2):"",0,0,1);
            
            if(isset($ruikei_point_tmp['rurubu']['location']) && round($ruikei_point_tmp['rurubu']['location'],0) != 0){
                $excel->addNumber($sheet_count,21,6,round($ruikei_point_tmp['rurubu']['location'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,21,6,"-",0,0,1);
            }
            //$excel->addNumber($sheet_count,35,7,"",0,0,1);
        }
        // //tripadvisor
        if(isset($ruikei_point_tmp['tripadvisor']) && count($ruikei_point_tmp['tripadvisor'])>0){
            
            if(isset($ruikei_point_tmp['tripadvisor']['service']) && round($ruikei_point_tmp['tripadvisor']['service'],0) != 0){
                $excel->addNumber($sheet_count,17,7,round($ruikei_point_tmp['tripadvisor']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,17,7,"-",0,0,1);
            }
            $excel->addString($sheet_count,18,7,"-",0,0,1);
            
            if(isset($ruikei_point_tmp['tripadvisor']['clean']) && round($ruikei_point_tmp['tripadvisor']['clean'],0) != 0){
                $excel->addNumber($sheet_count,19,7,round($ruikei_point_tmp['tripadvisor']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,19,7,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            
            if($ruikei_point_tmp['tripadvisor']['bedroom']!=0){
                $v1 = round($ruikei_point_tmp['tripadvisor']['bedroom'],2);
                $cnt++;
            }
            if($ruikei_point_tmp['tripadvisor']['room']!=0){
                $v2 = round($ruikei_point_tmp['tripadvisor']['room'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,20,7,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,20,7,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,31,8,$ruikei_point_tmp['tripadvisor']['bedroom']!=0?round($ruikei_point_tmp['tripadvisor']['bedroom'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,32,8,$ruikei_point_tmp['tripadvisor']['room']!=0?round($ruikei_point_tmp['tripadvisor']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,8,"",0,0,1);
            
            $cnt2=0;
            $vv1 = '';
            $vv2 = '';
            
            if($ruikei_point_tmp['tripadvisor']['location']!=0){
                $vv1 = round($ruikei_point_tmp['tripadvisor']['location'],2);
                $cnt2++;
            }
            if($ruikei_point_tmp['tripadvisor']['price']!=0){
                $vv2 = round($ruikei_point_tmp['tripadvisor']['price'],2);
                $cnt2++;
            }
            if($cnt2!=0){
                $excel->addNumber($sheet_count,21,7,round(($vv1+$vv2)/$cnt2,2),0,0,1);
            }else{
                $excel->addString($sheet_count,21,7,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,34,8,$ruikei_point_tmp['tripadvisor']['location']!=0?round($ruikei_point_tmp['tripadvisor']['location'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,35,8,$ruikei_point_tmp['tripadvisor']['price']!=0?round($ruikei_point_tmp['tripadvisor']['price'],2):"",0,0,1);
        }
// 
        // //expedia
        if(isset($ruikei_point_tmp['expedia']) && count($ruikei_point_tmp['expedia'])>0){
                
            if(isset($ruikei_point_tmp['expedia']['service']) && round($ruikei_point_tmp['expedia']['service'],0) != 0){
                $excel->addNumber($sheet_count,17,8,round($ruikei_point_tmp['expedia']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,17,8,"-",0,0,1);
            }    
            $excel->addString($sheet_count,18,8,"-",0,0,1);
            
            if(isset($ruikei_point_tmp['expedia']['clean']) && round($ruikei_point_tmp['expedia']['clean'],0) != 0){
                $excel->addNumber($sheet_count,19,8,round($ruikei_point_tmp['expedia']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,19,8,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            if($ruikei_point_tmp['expedia']['room']!=0){
                $v1 = round($ruikei_point_tmp['expedia']['room'],2);
                $cnt++;
            }
            if($ruikei_point_tmp['expedia']['conditions']!=0){
                $v2 = round($ruikei_point_tmp['expedia']['conditions'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,20,8,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,20,8,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,31,9,$ruikei_point_tmp['expedia']['room']!=0?round($ruikei_point_tmp['expedia']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,32,9,$ruikei_point_tmp['expedia']['conditions']!=0?round($ruikei_point_tmp['expedia']['conditions'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,9,"",0,0,1);
            $excel->addString($sheet_count,21,8,"-",0,0,1);
        }
        // //booking
        if(isset($ruikei_point_tmp['booking']) && count($ruikei_point_tmp['booking'])>0){
            
            if(isset($ruikei_point_tmp['booking']['staff']) && round($ruikei_point_tmp['booking']['staff'],0) != 0){
                $excel->addNumber($sheet_count,17,9,round($ruikei_point_tmp['booking']['staff']/2,2),0,0,1);    
            }else{
                $excel->addString($sheet_count,17,9,"-",0,0,1);
            }
            $excel->addString($sheet_count,18,9,"-",0,0,1);
            
            if(isset($ruikei_point_tmp['booking']['clean']) && round($ruikei_point_tmp['booking']['clean'],0) != 0){
                $excel->addNumber($sheet_count,19,9,round($ruikei_point_tmp['booking']['clean']/2,2),0,0,1);    
            }else{
                $excel->addString($sheet_count,19,9,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            if($ruikei_point_tmp['booking']['comfort']!=0){
                $v1 = round($ruikei_point_tmp['booking']['comfort'],2);
                $cnt++;
            }
            
            if($ruikei_point_tmp['booking']['amenity']!=0){
                $v2 = round($ruikei_point_tmp['booking']['amenity'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,20,9,round((($v1+$v2)/$cnt)/2,2),0,0,1);
            }else{
                $excel->addString($sheet_count,20,9,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,31,10,$ruikei_point_tmp['booking']['comfort']!=0?round($ruikei_point_tmp['booking']['comfort']/2,2):"",0,0,1);
            // $excel->addNumber($sheet_count,32,10,$ruikei_point_tmp['booking']['amenity']!=0?round($ruikei_point_tmp['booking']['amenity']/2,2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,10,"",0,0,1);
            
            $cnt2=0;
            $vv1 = '';
            $vv2 = '';
            
            if($ruikei_point_tmp['booking']['location']!=0){
                $vv1 = round($ruikei_point_tmp['booking']['location'],2);
                $cnt2++;
            }
            if($ruikei_point_tmp['booking']['value']!=0){
                $vv2 = round($ruikei_point_tmp['booking']['value'],2);
                $cnt2++;
            }
            if($cnt2!=0){
                $excel->addNumber($sheet_count,21,9,round((($vv1+$vv2)/$cnt2)/2,2),0,0,1);
            }else{
                $excel->addString($sheet_count,21,9,"-",0,0,1);    
            }
            //$excel->addNumber($sheet_count,34,10,$ruikei_point_tmp['booking']['location']!=0?round($ruikei_point_tmp['booking']['location']/2,2):"",0,0,1);
            //$excel->addNumber($sheet_count,35,10,$ruikei_point_tmp['booking']['value']!=0?round($ruikei_point_tmp['booking']['value']/2,2):"",0,0,1);
        }


        // //agoda
        if(isset($ruikei_point_tmp['agoda']) && count($ruikei_point_tmp['agoda'])>0){
            
            
            if(isset($ruikei_point_tmp['agoda']['staff']) && round($ruikei_point_tmp['agoda']['staff'],0) !=0){
                $excel->addNumber($sheet_count,17,10,round($ruikei_point_tmp['agoda']['staff']/2,2),0,0,1);    
            }else{
                $excel->addString($sheet_count,17,10,"-",0,0,1);
            }
            
            if(isset($ruikei_point_tmp['agoda']['food']) && round($ruikei_point_tmp['agoda']['food'],0) !=0){
                $excel->addNumber($sheet_count,18,10,round($ruikei_point_tmp['agoda']['food']/2,2),0,0,1);    
            }else{
                $excel->addString($sheet_count,18,10,"-",0,0,1);
            }
            
            if(isset($ruikei_point_tmp['agoda']['clean']) && round($ruikei_point_tmp['agoda']['clean'],0) !=0){
                $excel->addNumber($sheet_count,19,10,round($ruikei_point_tmp['agoda']['clean']/2,2),0,0,1);    
            }else{
                $excel->addString($sheet_count,19,10,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            if($ruikei_point_tmp['agoda']['room']!=0){
                $v1 = round($ruikei_point_tmp['agoda']['room'],2);
                $cnt++;
            }
            
            if($ruikei_point_tmp['agoda']['amenity']!=0){
                $v2 = round($ruikei_point_tmp['agoda']['amenity'],2);
                $cnt++;
            }
            if($cnt!=0 ){
                if($v1==""){
                    $v1 = 0;
                }
                if($v2==""){
                    $v2 = 0;
                }
                $tmp_sum = $v1+$v2;
                if($tmp_sum == 0){
                    $excel->addString($sheet_count,20,10,"-",0,0,1);
                }else{
                    $excel->addNumber($sheet_count,20,10,round((($v1+$v2)/$cnt)/2,2),0,0,1);
                }
                
            }else{
                $excel->addString($sheet_count,20,10,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,31,11,$ruikei_point_tmp['agoda']['room']!=0?round($ruikei_point_tmp['agoda']['room']/2,2):"",0,0,1);
            // $excel->addNumber($sheet_count,32,11,$ruikei_point_tmp['agoda']['amenity']!=0?round($ruikei_point_tmp['agoda']['amenity']/2,2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,11,"",0,0,1);

            $cnt2=0;
            $vv1 = '';
            $vv2 = '';
            
            if($ruikei_point_tmp['agoda']['location']!=0){
                $vv1 = round($ruikei_point_tmp['agoda']['location'],2);
                $cnt2++;
            }
            if($ruikei_point_tmp['agoda']['price']!=0){
                $vv2 = round($ruikei_point_tmp['agoda']['price'],2);
                $cnt2++;
            }
            if($cnt2!=0){
                $excel->addNumber($sheet_count,21,10,round((($vv1+$vv2)/$cnt2)/2,2),0,0,1);
            }else{
                $excel->addString($sheet_count,21,10,"-",0,0,1);    
            }
            
            //$excel->addNumber($sheet_count,34,11,$ruikei_point_tmp['agoda']['location']!=0?round($ruikei_point_tmp['agoda']['location']/2,2):"",0,0,1);
            //$excel->addNumber($sheet_count,35,11,$ruikei_point_tmp['agoda']['price']!=0?round($ruikei_point_tmp['agoda']['price']/2,2):"",0,0,1);
        }
        
       
        
        
        //3-1 口コミ分析 
        //件数
        if(isset($kuchikomi_avg_point_tmp['rakuten']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['rakuten']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,4,$kuchikomi_avg_point_tmp['rakuten']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,4,"-",0,0,1);
        } 
        if(isset($kuchikomi_avg_point_tmp['jalan']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['jalan']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,5,$kuchikomi_avg_point_tmp['jalan']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,5,"-",0,0,1);
        } 
        if(isset($kuchikomi_avg_point_tmp['rurubu']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['rurubu']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,6,$kuchikomi_avg_point_tmp['rurubu']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,6,"-",0,0,1);
        }    
        if(isset($kuchikomi_avg_point_tmp['tripadvisor']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['tripadvisor']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,7,$kuchikomi_avg_point_tmp['tripadvisor']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,7,"-",0,0,1);
        }    
      
        if(isset($kuchikomi_avg_point_tmp['expedia']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['expedia']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,8,$kuchikomi_avg_point_tmp['expedia']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,8,"-",0,0,1);
        }    
        
        if(isset($kuchikomi_avg_point_tmp['booking']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['booking']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,9,$kuchikomi_avg_point_tmp['booking']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,9,"-",0,0,1);
        }    
        
        if(isset($kuchikomi_avg_point_tmp['agoda']['kuchikomi_cnt']) && $kuchikomi_avg_point_tmp['agoda']['kuchikomi_cnt'] != 0){
            $excel->addNumber($sheet_count,27,10,$kuchikomi_avg_point_tmp['agoda']['kuchikomi_cnt']);
        }else{
            $excel->addString($sheet_count,27,10,"-",0,0,1);
        }   
      
        //3-1 口コミ分析 現在
        if(isset($kuchikomi_avg_point_tmp['rakuten']) && count($kuchikomi_avg_point_tmp['rakuten'])>0){
            if(isset($kuchikomi_avg_point_tmp['rakuten']['service']) && round($kuchikomi_avg_point_tmp['rakuten']['service'],0) != 0){
                $excel->addNumber($sheet_count,29,4,round($kuchikomi_avg_point_tmp['rakuten']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,29,4,"-",0,0,1);
            }    
            
            if(isset($kuchikomi_avg_point_tmp['rakuten']['food']) && round($kuchikomi_avg_point_tmp['rakuten']['food'],0) != 0){
                $excel->addNumber($sheet_count,30,4,round($kuchikomi_avg_point_tmp['rakuten']['food'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,30,4,"-",0,0,1);
            }
            $excel->addString($sheet_count,31,4,"-",0,0,1);
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            $v3 = '';
            if($kuchikomi_avg_point_tmp['rakuten']['room']!=0){
                $v1 = round($kuchikomi_avg_point_tmp['rakuten']['room'],2);
                $cnt++;
            }
            
            if($kuchikomi_avg_point_tmp['rakuten']['amenity']!=0){
                $v2 = round($kuchikomi_avg_point_tmp['rakuten']['amenity'],2);
                $cnt++;
            }
            
            if($kuchikomi_avg_point_tmp['rakuten']['bathroom']!=0){
                $v3 = round($kuchikomi_avg_point_tmp['rakuten']['bathroom'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,32,4,round(($v1+$v2+$v3)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,32,4,"-",0,0,1);    
            }
           
           if(isset($kuchikomi_avg_point_tmp['rakuten']['location'])  && round($kuchikomi_avg_point_tmp['rakuten']['location'],0) != 0){
               $excel->addNumber($sheet_count,33,4,round($kuchikomi_avg_point_tmp['rakuten']['location'],2));
           }else{
               $excel->addString($sheet_count,33,4,"-",6,0,1);
           }
        }
        // //jalan
        if(isset($kuchikomi_avg_point_tmp['jalan']) && count($kuchikomi_avg_point_tmp['jalan'])>0){
            
            if(isset($kuchikomi_avg_point_tmp['jalan']['service']) && round($kuchikomi_avg_point_tmp['jalan']['service'],0) != 0){
                $excel->addNumber($sheet_count,29,5,round($kuchikomi_avg_point_tmp['jalan']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,29,5,"-",0,0,1);   
            }
            if(isset($kuchikomi_avg_point_tmp['jalan']['breakfast']) && round($kuchikomi_avg_point_tmp['jalan']['breakfast'],0) !=0){
                $excel->addNumber($sheet_count,30,5,round($kuchikomi_avg_point_tmp['jalan']['breakfast'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,30,5,"-",0,0,1);    
            }
            
            if(isset($kuchikomi_avg_point_tmp['jalan']['clean']) && round($kuchikomi_avg_point_tmp['jalan']['clean'],0) != 0){
                $excel->addNumber($sheet_count,31,5,round($kuchikomi_avg_point_tmp['jalan']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,31,5,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            
            if($kuchikomi_avg_point_tmp['jalan']['room']!=0){
                $v1 = round($kuchikomi_avg_point_tmp['jalan']['room'],2);
                $cnt++;
            }
            if($kuchikomi_avg_point_tmp['jalan']['bathroom']!=0){
                $v2 = round($kuchikomi_avg_point_tmp['jalan']['bathroom'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,32,5,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,32,5,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,43,6,$kuchikomi_avg_point_tmp['jalan']['room']!=0?round($kuchikomi_avg_point_tmp['jalan']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,44,6,"",0,0,1);
            // $excel->addNumber($sheet_count,33,6,$kuchikomi_avg_point_tmp['jalan']['bathroom']!=0?round($kuchikomi_avg_point_tmp['jalan']['bathroom'],2):"",0,0,1);
            
            $excel->addString($sheet_count,33,5,"-",6,0,1);
        }
        // //rurubu
        if(isset($kuchikomi_avg_point_tmp['rurubu']) && count($kuchikomi_avg_point_tmp['rurubu'])>0){
            if(isset($kuchikomi_avg_point_tmp['rurubu']['service']) && round($kuchikomi_avg_point_tmp['rurubu']['service'],0) != 0){
                $excel->addNumber($sheet_count,29,6,round($kuchikomi_avg_point_tmp['rurubu']['service'],2),0,0,1);
            }else{
                $excel->addString($sheet_count,29,6,"-",0,0,1);
            }
            if(isset($kuchikomi_avg_point_tmp['rurubu']['food']) && round($kuchikomi_avg_point_tmp['rurubu']['food'],0) != 0){
                $excel->addNumber($sheet_count,30,6,round($kuchikomi_avg_point_tmp['rurubu']['food'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,30,6,"-",0,0,1);
            }
            $excel->addString($sheet_count,31,6,"-",0,0,1);
            
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            $v3 = '';
            if($kuchikomi_avg_point_tmp['rurubu']['room']!=0){
                $v1 = round($kuchikomi_avg_point_tmp['rurubu']['room'],2);
                $cnt++;
            }
            
            if($kuchikomi_avg_point_tmp['rurubu']['amenity']!=0){
                $v2 = round($kuchikomi_avg_point_tmp['rurubu']['amenity'],2);
                $cnt++;
            }
            
            if($kuchikomi_avg_point_tmp['rurubu']['bathroom']!=0){
                $v3 = round($kuchikomi_avg_point_tmp['rurubu']['bathroom'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,32,6,round(($v1+$v2+$v3)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,32,6,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,43,7,$kuchikomi_avg_point_tmp['rurubu']['room']!=0?round($kuchikomi_avg_point_tmp['rurubu']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,44,7,$kuchikomi_avg_point_tmp['rurubu']['amenity']!=0?round($kuchikomi_avg_point_tmp['rurubu']['amenity'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,7,$kuchikomi_avg_point_tmp['rurubu']['bathroom']!=0?round($kuchikomi_avg_point_tmp['rurubu']['bathroom'],2):"",0,0,1);
            
            if(isset($kuchikomi_avg_point_tmp['rurubu']['location']) && round($kuchikomi_avg_point_tmp['rurubu']['location'],0) != 0){
                $excel->addNumber($sheet_count,33,6,round($kuchikomi_avg_point_tmp['rurubu']['location'],2));    
            }else{
                $excel->addString($sheet_count,33,6,"-",6,0,1);
            }
            //$excel->addNumber($sheet_count,35,7,"",0,0,1);
        }
        // //tripadvisor
        if(isset($kuchikomi_avg_point_tmp['tripadvisor']) && count($kuchikomi_avg_point_tmp['tripadvisor'])>0){
            
            if(isset($kuchikomi_avg_point_tmp['tripadvisor']['service']) && round($kuchikomi_avg_point_tmp['tripadvisor']['service'],0) != 0){
                $excel->addNumber($sheet_count,29,7,round($kuchikomi_avg_point_tmp['tripadvisor']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,29,7,"-",0,0,1);
            }
            $excel->addString($sheet_count,30,7,"-",0,0,1);
            
            if(isset($kuchikomi_avg_point_tmp['tripadvisor']['clean']) && round($kuchikomi_avg_point_tmp['tripadvisor']['clean'],0) != 0){
                $excel->addNumber($sheet_count,31,7,round($kuchikomi_avg_point_tmp['tripadvisor']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,31,7,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            
            if($kuchikomi_avg_point_tmp['tripadvisor']['bedroom']!=0){
                $v1 = round($kuchikomi_avg_point_tmp['tripadvisor']['bedroom'],2);
                $cnt++;
            }
            if($kuchikomi_avg_point_tmp['tripadvisor']['room']!=0){
                $v2 = round($kuchikomi_avg_point_tmp['tripadvisor']['room'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,32,7,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,32,7,"-",6,0,1);
            }
            // $excel->addNumber($sheet_count,43,8,$kuchikomi_avg_point_tmp['tripadvisor']['bedroom']!=0?round($kuchikomi_avg_point_tmp['tripadvisor']['bedroom'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,44,8,$kuchikomi_avg_point_tmp['tripadvisor']['room']!=0?round($kuchikomi_avg_point_tmp['tripadvisor']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,8,"",0,0,1);
            
            $cnt2=0;
            $vv1 = '';
            $vv2 = '';
            
            
            
            if($kuchikomi_avg_point_tmp['tripadvisor']['location']!=0){
                $vv1 = round($kuchikomi_avg_point_tmp['tripadvisor']['location'],2);
                $cnt2++;
            }
            if($kuchikomi_avg_point_tmp['tripadvisor']['price']!=0){
                $vv2 = round($kuchikomi_avg_point_tmp['tripadvisor']['price'],2);
                $cnt2++;
            }
            
            if($cnt2!=0){
                $excel->addNumber($sheet_count,33,7,round(($vv1+$vv2)/$cnt2,2));
            }else{
                $excel->addString($sheet_count,33,7,"-",6,0,1);    
            }
            // $excel->addNumber($sheet_count,34,8,$kuchikomi_avg_point_tmp['tripadvisor']['location']!=0?round($kuchikomi_avg_point_tmp['tripadvisor']['location'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,35,8,$kuchikomi_avg_point_tmp['tripadvisor']['price']!=0?round($kuchikomi_avg_point_tmp['tripadvisor']['price'],2):"",0,0,1);
        }
// 
        // //expedia
        if(isset($kuchikomi_avg_point_tmp['expedia']) && count($kuchikomi_avg_point_tmp['expedia'])>0){
                
            if(isset($kuchikomi_avg_point_tmp['expedia']['service']) && round($kuchikomi_avg_point_tmp['expedia']['service'],0) != 0){
                $excel->addNumber($sheet_count,29,8,round($kuchikomi_avg_point_tmp['expedia']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,29,8,"-",0,0,1);
            }    
            $excel->addString($sheet_count,30,8,"-",0,0,1);
            
            if(isset($kuchikomi_avg_point_tmp['expedia']['clean']) && round($kuchikomi_avg_point_tmp['expedia']['clean'],0) != 0){
                $excel->addNumber($sheet_count,31,8,round($kuchikomi_avg_point_tmp['expedia']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,31,8,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = 0;
            $v2 = 0;
            if($kuchikomi_avg_point_tmp['expedia']['room']!=0){
                $v1 = round($kuchikomi_avg_point_tmp['expedia']['room'],2);
                $cnt++;
            }
            if($kuchikomi_avg_point_tmp['expedia']['conditions']!=0){
                $v2 = round($kuchikomi_avg_point_tmp['expedia']['conditions'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,32,8,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,32,8,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,43,9,$kuchikomi_avg_point_tmp['expedia']['room']!=0?round($kuchikomi_avg_point_tmp['expedia']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,44,9,$kuchikomi_avg_point_tmp['expedia']['conditions']!=0?round($kuchikomi_avg_point_tmp['expedia']['conditions'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,9,"",0,0,1);
            $excel->addString($sheet_count,33,8,"-",6,0,1);
        }
        // //booking
        if(isset($kuchikomi_avg_point_tmp['booking']) && count($kuchikomi_avg_point_tmp['booking'])>0){
            $excel->addString($sheet_count,29,9,"-",0,0,1);
            $excel->addString($sheet_count,30,9,"-",0,0,1);
            $excel->addString($sheet_count,31,9,"-",0,0,1);
            $excel->addString($sheet_count,32,9,"-",0,0,1);
            $excel->addString($sheet_count,33,9,"-",6,0,1);
            
        }
        //agoda
        if(isset($kuchikomi_avg_point_tmp['agoda']) && count($kuchikomi_avg_point_tmp['agoda'])>0){
            $excel->addString($sheet_count,29,10,"-",0,0,1);
            $excel->addString($sheet_count,30,10,"-",0,0,1);
            $excel->addString($sheet_count,31,10,"-",0,0,1);
            $excel->addString($sheet_count,32,10,"-",0,0,1);
            $excel->addString($sheet_count,33,10,"-",6,0,1);
        } 
        
        //3-2 口コミ分析 前回
        if(isset($kuchikomi_before_avg_point_tmp['rakuten']) && count($kuchikomi_before_avg_point_tmp['rakuten'])>0){
            if(isset($kuchikomi_before_avg_point_tmp['rakuten']['service']) && round($kuchikomi_before_avg_point_tmp['rakuten']['service'],0) != 0){
                $excel->addNumber($sheet_count,35,4,round($kuchikomi_before_avg_point_tmp['rakuten']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,35,4,"-",0,0,1);
            }    
            
            if(isset($kuchikomi_before_avg_point_tmp['rakuten']['food']) && round($kuchikomi_before_avg_point_tmp['rakuten']['food'],0) != 0){
                $excel->addNumber($sheet_count,36,4,round($kuchikomi_before_avg_point_tmp['rakuten']['food'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,36,4,"-",0,0,1);
            }
            $excel->addString($sheet_count,37,4,"-",0,0,1);
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            $v3 = '';
            if($kuchikomi_before_avg_point_tmp['rakuten']['room']!=0){
                $v1 = round($kuchikomi_before_avg_point_tmp['rakuten']['room'],2);
                $cnt++;
            }
            
            if($kuchikomi_before_avg_point_tmp['rakuten']['amenity']!=0){
                $v2 = round($kuchikomi_before_avg_point_tmp['rakuten']['amenity'],2);
                $cnt++;
            }
            
            if($kuchikomi_before_avg_point_tmp['rakuten']['bathroom']!=0){
                $v3 = round($kuchikomi_before_avg_point_tmp['rakuten']['bathroom'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,38,4,round(($v1+$v2+$v3)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,38,4,"-",0,0,1);    
            }
           
           if(isset($kuchikomi_before_avg_point_tmp['rakuten']['location'])  && round($kuchikomi_before_avg_point_tmp['rakuten']['location'],0) != 0){
               $excel->addNumber($sheet_count,39,4,round($kuchikomi_before_avg_point_tmp['rakuten']['location'],2),0,0,1);
           }else{
               $excel->addString($sheet_count,39,4,"-",0,0,1);
           }
        }
        
        // //jalan
        if(isset($kuchikomi_before_avg_point_tmp['jalan']) && count($kuchikomi_before_avg_point_tmp['jalan'])>0){
            
            if(isset($kuchikomi_before_avg_point_tmp['jalan']['service']) && round($kuchikomi_before_avg_point_tmp['jalan']['service'],0) != 0){
                $excel->addNumber($sheet_count,35,5,round($kuchikomi_before_avg_point_tmp['jalan']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,35,5,"-",0,0,1);   
            }
            if(isset($kuchikomi_before_avg_point_tmp['jalan']['breakfast']) && round($kuchikomi_before_avg_point_tmp['jalan']['breakfast'],0) !=0){
                $excel->addNumber($sheet_count,36,5,round($kuchikomi_before_avg_point_tmp['jalan']['breakfast'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,36,5,"-",0,0,1);    
            }
            
            if(isset($kuchikomi_before_avg_point_tmp['jalan']['clean']) && round($kuchikomi_before_avg_point_tmp['jalan']['clean'],0) != 0){
                $excel->addNumber($sheet_count,37,5,round($kuchikomi_before_avg_point_tmp['jalan']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,37,5,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            
            if($kuchikomi_before_avg_point_tmp['jalan']['room']!=0){
                $v1 = round($kuchikomi_before_avg_point_tmp['jalan']['room'],2);
                $cnt++;
            }
            if($kuchikomi_before_avg_point_tmp['jalan']['bathroom']!=0){
                $v2 = round($kuchikomi_before_avg_point_tmp['jalan']['bathroom'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,38,5,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,38,5,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,49,6,$kuchikomi_before_avg_point_tmp['jalan']['room']!=0?round($kuchikomi_before_avg_point_tmp['jalan']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,50,6,"",0,0,1);
            // $excel->addNumber($sheet_count,33,6,$kuchikomi_before_avg_point_tmp['jalan']['bathroom']!=0?round($kuchikomi_before_avg_point_tmp['jalan']['bathroom'],2):"",0,0,1);
            
            $excel->addString($sheet_count,39,5,"-",0,0,1);
        }
        // //rurubu
        if(isset($kuchikomi_before_avg_point_tmp['rurubu']) && count($kuchikomi_before_avg_point_tmp['rurubu'])>0){
            if(isset($kuchikomi_before_avg_point_tmp['rurubu']['service']) && round($kuchikomi_before_avg_point_tmp['rurubu']['service'],0) != 0){
                $excel->addNumber($sheet_count,35,6,round($kuchikomi_before_avg_point_tmp['rurubu']['service'],2),0,0,1);
            }else{
                $excel->addString($sheet_count,35,6,"-",0,0,1);
            }
            if(isset($kuchikomi_before_avg_point_tmp['rurubu']['food']) && round($kuchikomi_before_avg_point_tmp['rurubu']['food'],0) != 0){
                $excel->addNumber($sheet_count,36,6,round($kuchikomi_before_avg_point_tmp['rurubu']['food'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,36,6,"-",0,0,1);
            }
            $excel->addString($sheet_count,37,6,"-",0,0,1);
            
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            $v3 = '';
            if($kuchikomi_before_avg_point_tmp['rurubu']['room']!=0){
                $v1 = round($kuchikomi_before_avg_point_tmp['rurubu']['room'],2);
                $cnt++;
            }
            
            if($kuchikomi_before_avg_point_tmp['rurubu']['amenity']!=0){
                $v2 = round($kuchikomi_before_avg_point_tmp['rurubu']['amenity'],2);
                $cnt++;
            }
            
            if($kuchikomi_before_avg_point_tmp['rurubu']['bathroom']!=0){
                $v3 = round($kuchikomi_before_avg_point_tmp['rurubu']['bathroom'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,38,6,round(($v1+$v2+$v3)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,38,6,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,49,7,$kuchikomi_before_avg_point_tmp['rurubu']['room']!=0?round($kuchikomi_before_avg_point_tmp['rurubu']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,50,7,$kuchikomi_before_avg_point_tmp['rurubu']['amenity']!=0?round($kuchikomi_before_avg_point_tmp['rurubu']['amenity'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,7,$kuchikomi_before_avg_point_tmp['rurubu']['bathroom']!=0?round($kuchikomi_before_avg_point_tmp['rurubu']['bathroom'],2):"",0,0,1);
            
            if(isset($kuchikomi_before_avg_point_tmp['rurubu']['location']) && round($kuchikomi_before_avg_point_tmp['rurubu']['location'],0) != 0){
                $excel->addNumber($sheet_count,39,6,round($kuchikomi_before_avg_point_tmp['rurubu']['location'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,39,6,"-",0,0,1);
            }
            //$excel->addNumber($sheet_count,35,7,"",0,0,1);
        }
        // //tripadvisor
        if(isset($kuchikomi_before_avg_point_tmp['tripadvisor']) && count($kuchikomi_before_avg_point_tmp['tripadvisor'])>0){
            
            if(isset($kuchikomi_before_avg_point_tmp['tripadvisor']['service']) && round($kuchikomi_before_avg_point_tmp['tripadvisor']['service'],0) != 0){
                $excel->addNumber($sheet_count,35,7,round($kuchikomi_before_avg_point_tmp['tripadvisor']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,35,7,"-",0,0,1);
            }
            $excel->addString($sheet_count,36,7,"-",0,0,1);
            
            if(isset($kuchikomi_before_avg_point_tmp['tripadvisor']['clean']) && round($kuchikomi_before_avg_point_tmp['tripadvisor']['clean'],0) != 0){
                $excel->addNumber($sheet_count,37,7,round($kuchikomi_before_avg_point_tmp['tripadvisor']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,37,7,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = '';
            $v2 = '';
            
            if($kuchikomi_before_avg_point_tmp['tripadvisor']['bedroom']!=0){
                $v1 = round($kuchikomi_before_avg_point_tmp['tripadvisor']['bedroom'],2);
                $cnt++;
            }
            if($kuchikomi_before_avg_point_tmp['tripadvisor']['room']!=0){
                $v2 = round($kuchikomi_before_avg_point_tmp['tripadvisor']['room'],2);
                $cnt++;
            }
            
            if($cnt!=0){
                $excel->addNumber($sheet_count,38,7,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,38,7,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,49,8,$kuchikomi_before_avg_point_tmp['tripadvisor']['bedroom']!=0?round($kuchikomi_before_avg_point_tmp['tripadvisor']['bedroom'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,50,8,$kuchikomi_before_avg_point_tmp['tripadvisor']['room']!=0?round($kuchikomi_before_avg_point_tmp['tripadvisor']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,8,"",0,0,1);
            
            $cnt2=0;
            $vv1 = '';
            $vv2 = '';
            
            if($kuchikomi_before_avg_point_tmp['tripadvisor']['location']!=0){
                $vv1 = round($kuchikomi_before_avg_point_tmp['tripadvisor']['location'],2);
                $cnt2++;
            }
            if($kuchikomi_before_avg_point_tmp['tripadvisor']['price']!=0){
                $vv2 = round($kuchikomi_before_avg_point_tmp['tripadvisor']['price'],2);
                $cnt2++;
            }
            if($cnt2!=0){
                $excel->addNumber($sheet_count,39,7,round(($vv1+$vv2)/$cnt2,2),0,0,1);
            }else{
                $excel->addString($sheet_count,39,7,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,34,8,$kuchikomi_before_avg_point_tmp['tripadvisor']['location']!=0?round($kuchikomi_before_avg_point_tmp['tripadvisor']['location'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,35,8,$kuchikomi_before_avg_point_tmp['tripadvisor']['price']!=0?round($kuchikomi_before_avg_point_tmp['tripadvisor']['price'],2):"",0,0,1);
        }
// 
        // //expedia
        if(isset($kuchikomi_before_avg_point_tmp['expedia']) && count($kuchikomi_before_avg_point_tmp['expedia'])>0){
                
            if(isset($kuchikomi_before_avg_point_tmp['expedia']['service']) && round($kuchikomi_before_avg_point_tmp['expedia']['service'],0) != 0){
                $excel->addNumber($sheet_count,35,8,round($kuchikomi_before_avg_point_tmp['expedia']['service'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,35,8,"-",0,0,1);
            }    
            $excel->addString($sheet_count,36,8,"-",0,0,1);
            
            if(isset($kuchikomi_before_avg_point_tmp['expedia']['clean']) && round($kuchikomi_before_avg_point_tmp['expedia']['clean'],0) != 0){
                $excel->addNumber($sheet_count,37,8,round($kuchikomi_before_avg_point_tmp['expedia']['clean'],2),0,0,1);    
            }else{
                $excel->addString($sheet_count,37,8,"-",0,0,1);
            }
            
            $cnt=0;
            $v1 = 0;
            $v2 = 0;
            if($kuchikomi_before_avg_point_tmp['expedia']['room']!=0){
                $v1 = round($kuchikomi_before_avg_point_tmp['expedia']['room'],2);
                $cnt++;
            }
            if($kuchikomi_before_avg_point_tmp['expedia']['conditions']!=0){
                $v2 = round($kuchikomi_before_avg_point_tmp['expedia']['conditions'],2);
                $cnt++;
            }
            if($cnt!=0){
                $excel->addNumber($sheet_count,38,8,round(($v1+$v2)/$cnt,2),0,0,1);
            }else{
                $excel->addString($sheet_count,38,8,"-",0,0,1);    
            }
            // $excel->addNumber($sheet_count,49,9,$kuchikomi_before_avg_point_tmp['expedia']['room']!=0?round($kuchikomi_before_avg_point_tmp['expedia']['room'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,50,9,$kuchikomi_before_avg_point_tmp['expedia']['conditions']!=0?round($kuchikomi_before_avg_point_tmp['expedia']['conditions'],2):"",0,0,1);
            // $excel->addNumber($sheet_count,33,9,"",0,0,1);
            $excel->addString($sheet_count,39,8,"-",0,0,1);
        }
        // //booking
        if(isset($kuchikomi_before_avg_point_tmp['booking']) && count($kuchikomi_before_avg_point_tmp['booking'])>0){
            $excel->addString($sheet_count,35,9,"-",0,0,1);
            $excel->addString($sheet_count,36,9,"-",0,0,1);
            $excel->addString($sheet_count,37,9,"-",0,0,1);
            $excel->addString($sheet_count,38,9,"-",0,0,1);
            $excel->addString($sheet_count,39,9,"-",0,0,1);
            
        }
        //agoda
        if(isset($kuchikomi_before_avg_point_tmp['agoda']) && count($kuchikomi_before_avg_point_tmp['agoda'])>0){
            $excel->addString($sheet_count,35,10,"-",0,0,1);
            $excel->addString($sheet_count,36,10,"-",0,0,1);
            $excel->addString($sheet_count,37,10,"-",0,0,1);
            $excel->addString($sheet_count,38,10,"-",0,0,1);
            $excel->addString($sheet_count,39,10,"-",0,0,1);
        }
        
             
        // テンプレートシート削除
        $excel->rmSheet(1);
        // シート名
        $excel->setSheetname($sheet_count,$hotel_name);
        
        // // セルを削除する例です。
        // $readfile ='../../excel/tokyustay/tokyustaytemp_new.xls'; // テンプレートファイルの指定
        // //test
        // $readfile='D:\myProject\bitbucket\rc19_tokyustay\public\storages\temp\r1_tokyustaytemp_new.xls'; 
//         
        // $outfile  = "tokyustay".date("ymdHi",strtotime(date("Y-m-d H:i:s"))).".xls";           // 出力するファイル名です
        // // 最後に書換えを実行します
        // $excel->reviseFile($readfile,$outfile);
        // return true;
        
        $file_name="r1_tokyustaytemp_new.xls"; 
        $outfile  = "tokyustay".date("ymdHi",strtotime(date("Y-m-d H:i:s"))).".xls";           // 出力するファイル名です
        
        $report = "temp";
        $save_report = "excel";
        
        $s3_dir_pash = $report.'/';
        $s3_file_pash = $s3_dir_pash.$file_name; 
        $kind_report = "tokyustay";
        
        $readfile_contents = Storage::disk('s3_tokyustay')->get($s3_file_pash);
        Storage::disk($kind_report)->put($file_name, $readfile_contents);
        
        $path = storage_path();
        $excel_path = $path."/app/".$kind_report."/";
        $excel->reviseFile($excel_path.$file_name,$outfile);
        
        // $contents = Storage::disk($kind_report)->get($file_name);
        // Storage::disk('s3_tokyuhotels')->put('excel/rpt01/'.$outfile, $contents);
        
        echo 'TOKYUSTAY-REPORT-01-DONE'."\n";
        exit;
        
        
        
    }

    //2.クチコミ評価分析（バリューチェーン）
    public static function TokyuStayReportValuechain(Request $request){
        
        $data = $request::all();
        
        ini_set("memory_limit", "-1");
        ini_set("max_execution_time",0); // タイムアウトしない
        ini_set("max_input_time",0); // パース時間を設定しない
        
                    
        if(isset($data['start_date'])){
            $date_from = $data['start_date'];
        }else{
            $date_from = date("Y-m-d");
        }
        
        if(isset($data['end_date'])){
            $date_to = $data['end_date'];
        }else{
            $date_to = date("Y-m-d");
        }
        
        if(isset($data['hotel_id'])){
            $keyword_mst_seq = $data['hotel_id'];
        }else{
            $keyword_mst_seq = '61635';
        } 
        
        $excel = new ExcelReviser;
        $excel->setInternalCharset('utf-8');
        
        
        $site_info = self::getRankingSites();
        $sex_type = self::getSex();
        $today_date = date("Y-m-d");
        
        $hotel_name = self::getHotelName($keyword_mst_seq);
        
        //4. 項目別コメントgetHotelEffectCountByKuchikomiScoreValuechain
        $tmp = array ();
        //$hotel_effect_arr_tmp = array();
        $hotel_effect_arr_tmp = self::getHotelEffectCountByKuchikomiScoreValuechain('4633',$date_from,$date_to,$keyword_mst_seq);
        
                
        //部屋・設備・アメニティ等
        $act_name_array = array("接客・サービス","朝食","清掃","部屋・設備・アメニティ等","その他（立地、料金等）");
        $posi_array = array("positive","negative","request",);
        $hotel_effect_arr = array();
        //sort act_nameのごとにソート
        
        
        if(count($hotel_effect_arr_tmp) >0){
            foreach ($hotel_effect_arr_tmp as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    $hotel_effect_arr[$v1['act_name']][$k][] =$v1;
                }
            }
        }
        
        
        $hotel_effect_arr_tmp = array();
        if(count($hotel_effect_arr) >0){
            foreach ($hotel_effect_arr as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    $duplication_flag = array();
                    foreach ($v2 as $k3 => $v3) {
                        if(!isset($hotel_effect_arr_tmp[$k1][$k2][$v3['mng_kuchikomi_mst_seq']])){
                                $hotel_effect_arr_tmp[$k1][$k2][$v3['mng_kuchikomi_mst_seq']] = $v3;
                        }
                    }
                      
                }
            }
        }
        
        //e($result);
        //exit;
       //e($hotel_effect_arr_tmp['接客・サービス']['negative']);exit;
       
        
        $hotel_effect_arr_ok = array();
        if(count($hotel_effect_arr_tmp) >0){
            foreach ($hotel_effect_arr_tmp as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    foreach ($v2 as $k3 => $v3) {
                        $hotel_effect_arr_ok[$k1][$k2][] = $v3;
                    }
                }
            }
        }
              
        $positive_num = 0;
        $negative_num = 0;
        $request_num = 0;
        
        $array_data = array();
        if(count($hotel_effect_arr_ok) >0){
            foreach ($hotel_effect_arr_ok as $k1 => $v1) {
                $duplication_check_service = array();
                $duplication_check_food = array();
                $duplication_check_clean = array();
                $duplication_check_room = array();
                $duplication_check_other = array();
                foreach ($v1 as $k2 => $v2) {
                    foreach ($v2 as $k3 => $v3) {
                        if($k1 == "接客・サービス"){
                            if($v3["site_id"] == "rakuten" || $v3["site_id"] == "rurubu"|| $v3["site_id"] == "jalan"){
                                if(isset($v3['service']) && $v3['service'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_service)){
                                        continue;
                                    }
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_service[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                                
                                if(isset($v3['service']) && $v3['service'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_service)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_service[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }else{
                                if($k2 == "positive"){
                                    $array_data[$k1][$k2][$positive_num] = $v3;
                                    $positive_num++;
                                }else if($k2 == "negative"){
                                    $array_data[$k1][$k2][$negative_num] = $v3;
                                    $negative_num++;
                                }else{
                                    $array_data[$k1][$k2][$request_num] = $v3;
                                    $request_num++;
                                }
                            }
                        }else if($k1 == "朝食"){
                            if($v3["site_id"] == "rakuten" || $v3["site_id"] == "rurubu"){
                                if(isset($v3['food']) && $v3['food'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_food)){
                                        continue;
                                    }
                                    
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_food[] = $v3['mng_kuchikomi_mst_seq'];
                                }else if(isset($v3['food']) && $v3['food'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_food)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_food[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }elseif($v3["site_id"] == "jalan"){
                                if(isset($v3['breakfast']) && $v3['breakfast'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_food)){
                                        continue;
                                    }
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_food[] = $v3['mng_kuchikomi_mst_seq'];
                                }else if(isset($v3['breakfast']) && $v3['breakfast'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_food)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_food[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }else{
                                 if($k2 == "positive"){
                                    $array_data[$k1][$k2][$positive_num] = $v3;
                                    $positive_num++;
                                }else if($k2 == "negative"){
                                    $array_data[$k1][$k2][$negative_num] = $v3;
                                    $negative_num++;
                                }else{
                                    $array_data[$k1][$k2][$request_num] = $v3;
                                    $request_num++;
                                }
                            }
                        }else if($k1 == "清掃"){
                            if($v3["site_id"] == "jalan"){
                                if(isset($v3['clean']) && $v3['clean'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_clean)){
                                        continue;
                                    }
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_clean[] = $v3['mng_kuchikomi_mst_seq'];
                                }else if(isset($v3['clean']) && $v3['clean'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_clean)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_clean[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }else{
                                if($k2 == "positive"){
                                    $array_data[$k1][$k2][$positive_num] = $v3;
                                    $positive_num++;
                                }else if($k2 == "negative"){
                                    $array_data[$k1][$k2][$negative_num] = $v3;
                                    $negative_num++;
                                }else{
                                    $array_data[$k1][$k2][$request_num] = $v3;
                                    $request_num++;
                                }
                            }
                        }else if($k1 == "部屋・設備・アメニティ等"){
                            if($v3["site_id"] == "rakuten" || $v3["site_id"] == "rurubu"){
                                if(isset($v3['amenity']) && $v3['amenity'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_room)){
                                        continue;
                                    }
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_room[] = $v3['mng_kuchikomi_mst_seq'];
                                }else if(isset($v3['amenity']) && $v3['amenity'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_room)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_room[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }elseif($v3["site_id"] == "jalan"){
                                if(isset($v3['room']) && $v3['room'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_room)){
                                        continue;
                                    }
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_room[] = $v3['mng_kuchikomi_mst_seq'];
                                }else if(isset($v3['room']) && $v3['room'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_room)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_room[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }else{
                                if($k2 == "positive"){
                                    $array_data[$k1][$k2][$positive_num] = $v3;
                                    $positive_num++;
                                }else if($k2 == "negative"){
                                    $array_data[$k1][$k2][$negative_num] = $v3;
                                    $negative_num++;
                                }else{
                                    $array_data[$k1][$k2][$request_num] = $v3;
                                    $request_num++;
                                }
                            }
                        }else if($k1 == "その他（立地、料金等）"){
                            if($v3["site_id"] == "rakuten" || $v3["site_id"] == "rurubu"){
                                if(isset($v3['location']) && $v3['location'] >= 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_other)){
                                        continue;
                                    }
                                    $array_data[$k1]['positive'][$positive_num] = $v3;
                                    $positive_num++;
                                    $duplication_check_other[] = $v3['mng_kuchikomi_mst_seq'];
                                }else if(isset($v3['location']) && $v3['location'] < 4){
                                    if(in_array($v3['mng_kuchikomi_mst_seq'], $duplication_check_other)){
                                        continue;
                                    }
                                    $array_data[$k1]['negative'][$negative_num] = $v3;
                                    $negative_num++;
                                    $duplication_check_other[] = $v3['mng_kuchikomi_mst_seq'];
                                }
                            }else{
                                if($k2 == "positive"){
                                    $array_data[$k1][$k2][$positive_num] = $v3;
                                    $positive_num++;
                                }else if($k2 == "negative"){
                                    $array_data[$k1][$k2][$negative_num] = $v3;
                                    $negative_num++;
                                }else{
                                    $array_data[$k1][$k2][$request_num] = $v3;
                                    $request_num++;
                                }
                            }
                        }else{
                            if($k2 == "positive"){
                                $array_data[$k1][$k2][$positive_num] = $v3;
                                $positive_num++;
                            }else if($k2 == "negative"){
                                $array_data[$k1][$k2][$negative_num] = $v3;
                                $negative_num++;
                            }else{
                                $array_data[$k1][$k2][$request_num] = $v3;
                                $request_num++;
                            }
                        }
                    }
                }    
            }
        }
       //駅から近くてよかったです。受付の方も感
        //e($array_data['接客・サービス']['positive']);exit;
        $hotel_effect = array();
        if(count($array_data) >0){
            foreach ($act_name_array as $key => $value) {
                foreach ($posi_array as $k_p => $v_p) {
                    if(isset($array_data[$value][$v_p])){
                        $hotel_effect[$value][$v_p] = $array_data[$value][$v_p];
                        
                    }
                    
                        
                }
            }
        }
        
        
        
         
        
        $hotel_effect_sort = array();
        foreach ($hotel_effect as $k1 => $v1) {
            
            
            foreach ($v1 as $k2 => $v2){
                $total = array();
                $total_list = array();
                
                foreach ($v2 as $k3 => $val){
                    if($k1 == "接客・サービス"){
                        if($val["site_id"] == "rakuten" || $val["site_id"] == "jalan" || $val["site_id"] == "rurubu"){
                            if(isset($val['service'])){
                                $total[$k3] = $val['service'];
                                $total_list[$k3] = $val;
                            } 
                        }else{
                            $total[$k3] = 0;
                            $total_list[$k3] = $val;
                        } 
                    }else if($k1 == "朝食"){
                        if($val["site_id"] == "rakuten" || $val["site_id"] == "rurubu"){
                            if(isset($val['food'])){
                                $total[$k3] = $val['food'];
                                $total_list[$k3] = $val;
                            } 
                        }elseif($val["site_id"] == "jalan" ){
                            if(isset($val['breakfast'])){
                                $total[$k3] = $val['breakfast'];
                                $total_list[$k3] = $val;
                            } 
                        }else{
                            $total[$k3] = 0;
                            $total_list[$k3] = $val;
                        }
                    }else if($k1 == "清掃"){
                        if($val["site_id"] == "jalan" ){
                            if(isset($val['clean'])){
                                $total[$k3] = $val['clean'];
                                $total_list[$k3] = $val;
                            } 
                        }else{
                            $total[$k3] = 0;
                            $total_list[$k3] = $val;
                        }
                        
                    }else if($k1 == "部屋・設備・アメニティ等"){
                        if($val["site_id"] == "rakuten" || $val["site_id"] == "rurubu"){
                            if(isset($val['amenity'])){
                                $total[$k3] = $val['amenity'];
                                $total_list[$k3] = $val;
                            } 
                        }elseif($val["site_id"] == "jalan" ){
                            if(isset($val['room'])){
                                $total[$k3] = $val['room'];
                                $total_list[$k3] = $val;
                            } 
                        }else{
                            $total[$k3] = 0;
                            $total_list[$k3] = $val;
                        }
                    }else if($k1 == "その他（立地、料金等）"){
                        if($val["site_id"] == "rakuten" || $val["site_id"] == "rurubu"){
                            if(isset($val['location'])){
                                $total[$k3] = $val['location'];
                                $total_list[$k3] = $val;
                            } 
                        }else{
                            $total[$k3] = 0;
                            $total_list[$k3] = $val;
                        }
                    }
                }
               
                if(count($total)!=0 ){
                    array_multisort($total, SORT_DESC ,SORT_NUMERIC, $total_list);
                    $hotel_effect_sort[$k1][$k2]  = $total_list;
                }
                
                
            }
        }
        
        
        $sheet_count = 0;
        $excel->addString($sheet_count,4,1,"店名 : ".$hotel_name,0,8,1);
        $excel->addString($sheet_count,1,6,"日付 : ".date('Y年m月d日'));
        $excel->addString($sheet_count,5,1,"期間 : ".date('Y年m月d日',strtotime($date_from))."～".date('Y年m月d日',strtotime($date_to)),0,8,1);
        
        //2-1 東急ステイ平均
        $ruikei_tokyustay_avg_tmp_col =5;
        //2-2-1 エリア平均総合
        $area_col = 5;
        //2-2-2 エリア順位
        $ruikei_area_avg_tmp_col = 5;
        //2-3 ＯＴＡ別評価点 現在
        $ruikei_point_tmp_col =5;
        //2-4 ＯＴＡ別評価点 前回 total
        $ruikei_before_point_tmp_col =5;
        
        //3-1 口コミ分析 現在
        $kuchikomi_avg_point_tmp_col = 5;
        //3-2 口コミ分析 前回
        $kuchikomi_before_avg_point_tmp_col = 5;
        
        
        $row = 7;
        $col = 1;
        
        if(count($hotel_effect_sort) != 0){
            
            foreach ($hotel_effect_sort as $k1 => $v1) {
                ++$row;
                $excel->addString($sheet_count,$row++,1,"■ ".$k1,0,6,1);
                //$row_negative_start = $row;
                foreach ($v1 as $k2 => $v2) {
                    
                    $excel->addString($sheet_count,$row,1,"ﾊﾞﾘｭｰﾁｪｰﾝ",2,0,1);
                    $excel->addString($sheet_count,$row,2,"項目点数",2,0,1);
                    $excel->addString($sheet_count,$row,3,"総合点数",2,0,1);
                    $excel->addString($sheet_count,$row,4,"媒体",2,0,1);
                    $excel->addString($sheet_count,$row,5,"属性",2,0,1);
                    $excel->addString($sheet_count,$row,6,"口コミ内容",2,0,1);           
                    
                    $row++;
                    $k2_row = $row;
                    $excel->addString($sheet_count,$k2_row,1,$k2,0,0,1);
                    foreach ($v2 as $k3 => $v3) {
                        $col2 = 2;
                        
                        
                        //$act_name_array = array("接客・サービス","朝食","清掃","部屋・設備・アメニティ等","その他（立地、料金等）");
                        
                        //接客・サービス
                        // rakuten :  service
                        // jalan : service
                        // rurubu : service
                        //朝食
                        // rakuten : food
                        // jalan :breakfast
                        // rurubu : food
                        //清掃
                        // rakuten : なし
                        // jalan : clean
                        // rurubu : 
                        //部屋・設備・アメニティ等
                        // rakuten : amenity
                        // jalan :room
                        // rurubu : amenity
                        //その他（立地、料金等）
                        // rakuten : location
                        // jalan :
                        // rurubu : location
                        
                        if($k1 == "接客・サービス"){
                            if($v3['site_id'] == "rakuten" || $v3['site_id'] == "jalan" || $v3['site_id'] == "rurubu"){
                                if(isset($v3['service']) && $v3['service'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['service'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else{
                                 $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                            }
                        }else if($k1 == "朝食"){
                            if($v3['site_id'] == "rakuten" || $v3['site_id'] == "rurubu"){
                                if(isset($v3['food']) && $v3['food'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['food'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else if($v3['site_id'] == "jalan"){
                                if(isset($v3['breakfast']) && $v3['breakfast'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['breakfast'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else{
                                $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                            }
                        }else if($k1 == "清掃"){
                            if($v3['site_id'] == "jalan"){
                                if(isset($v3['clean']) && $v3['clean'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['clean'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else{
                                $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                            }
                        }else if($k1 == "部屋・設備・アメニティ等"){
                            if($v3['site_id'] == "rakuten" || $v3['site_id'] == "rurubu"){
                                if(isset($v3['amenity']) && $v3['amenity'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['amenity'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else if($v3['site_id'] == "jalan"){
                                if(isset($v3['room']) && $v3['room'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['room'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else{
                                $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                            }
                        }else if($k1 == "その他（立地、料金等）"){
                            if($v3['site_id'] == "rakuten" || $v3['site_id'] == "rurubu"){
                                if(isset($v3['location']) && $v3['location'] > 0){
                                    $excel->addNumber($sheet_count,$row,$col2++,$v3['location'],0,0,1);
                                }else{
                                    $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                                }
                            }else{
                                $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                            }
                        }
                        
                        if($v3['total']>0){
                            if($v3['site_id'] == "booking" ||$v3['site_id'] == "agoda"){
                                $excel->addNumber($sheet_count,$row,$col2++,isset($v3['total'])?round($v3['total']/2,2):"",0,0,1);
                            }else{
                                $excel->addNumber($sheet_count,$row,$col2++,isset($v3['total'])?round($v3['total'],2):"",0,0,1);
                            }
                        }else{
                            $excel->addString($sheet_count,$row,$col2++,"-",0,0,1);
                        }
                        
                        $excel->addString($sheet_count,$row,$col2++,isset($v3['site_id'])?$site_info[$v3['site_id']]:"-",0,0,1);
                        $excel->addString($sheet_count,$row,$col2++,$v3['sex']!= ""?$sex_type[$v3['sex']]:"-",0,0,1);
$comment =<<<EOF

{$v3['comment']}

EOF;
                        $excel->addString($sheet_count,$row,$col2,$comment,0,0,1);
                        
                        $excel->addString($sheet_count,$row,1,$k2,0,0,1);
                        $row++;
                    }
                    $excel->setCellMerge($sheet_count,$k2_row,$row-1,1,1); 
                    //$row++;
                }
                //$row +=5;
            }
           
        }
        
        
        // テンプレートシート削除
        $excel->rmSheet(1);
        // シート名
        $excel->setSheetname($sheet_count,$hotel_name);
        
        // // セルを削除する例です。
        // $readfile ='../../excel/tokyustay/tokyustaytemp_valuechain_new.xls'; // テンプレートファイルの指定
        // //TODO SQL
        // $readfile='D:\myProject\bitbucket\rc19_tokyustay\public\storages\temp\r2_tokyustaytemp_valuechain_new.xls'; 
        // $outfile  = "tokyustay".date("ymdHi",strtotime(date("Y-m-d H:i:s"))).".xls";           // 出力するファイル名です
        // // 最後に書換えを実行します
        // $excel->reviseFile($readfile,$outfile);
        // return true;
        
        $file_name="r2_tokyustaytemp_valuechain_new.xls"; 
        $outfile  = "tokyustay".date("ymdHi",strtotime(date("Y-m-d H:i:s"))).".xls";           // 出力するファイル名です
        
        $report = "temp";
        $save_report = "excel";
        
        $s3_dir_pash = $report.'/';
        $s3_file_pash = $s3_dir_pash.$file_name; 
        $kind_report = "tokyustay";
        
        $readfile_contents = Storage::disk('s3_tokyustay')->get($s3_file_pash);
        Storage::disk($kind_report)->put($file_name, $readfile_contents);
        
        $path = storage_path();
        $excel_path = $path."/app/".$kind_report."/";
        $excel->reviseFile($excel_path.$file_name,$outfile);
        
        // $contents = Storage::disk($kind_report)->get($file_name);
        // Storage::disk('s3_tokyuhotels')->put('excel/rpt01/'.$outfile, $contents);
        
        echo 'TOKYUSTAY-REPORT-02-DONE'."\n";
        exit;
        
        
    }
    
    
    

   
}

?>