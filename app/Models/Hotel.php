<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use DB;
class Hotel extends Model
{
    protected $table   = 'ota_room_type';
    protected $connection   = 'rc_admin';
    
    public static function getHotelList()
    {
        
        $data = self::table('ota_room_type')->where('ota_id', 2)->get()->toArray();
                    

        return $data;
    }
}
