<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_ADMIN_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'rc_old' => [

            'driver' => 'mysql',
            'host' => env('OLD_RCDB_HOST', '127.0.0.1'),
            'port' => env('OLD_RCDB_PORT', '3306'),
            'database' => env('OLD_RCDB_DATABASE', 'forge'),
            'username' => env('OLD_RCDB_USERNAME', 'forge'),
            'password' => env('OLD_RCDB_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'admin' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_ADMIN_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'rc_admin' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_ADMIN_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'price' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_PRICE_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'valuechain_db' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_VALUECHAIN_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'reputation' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_REPUTATION_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'priceM' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_PRICE_M_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'reviewM' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_REVIEW_M_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'review' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_REVIEW_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'hotelM' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_HOTEL_M_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        /**
         * Package DB
         */

        'rc_core' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('RC_CORE_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'tokyustay' => [
            'driver' => 'mysql',
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('DB_TOKYUSTAY_CUSTOMIZE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
/*
        'rc_admin' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('RC_ADMIN_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
*/
        
        'com_price' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('COM_PRICE_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'com_vc' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('COM_VC_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'com_survey' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('COM_SURVEY_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
/*
        'hotelMaster' => [
            'driver' => 'mysql',
            'host' => env('HOTEL_MASTER_HOST', '127.0.0.1'),
            'port' => env('HOTEL_MASTER_PORT', '3306'),
            'database' => env('HOTEL_MASTER_DATABASE', 'forge'),
            'username' => env('HOTEL_MASTER_USERNAME', 'forge'),
            'password' => env('HOTEL_MASTER_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],
*/
        'daiwa' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('CUSTOMIZE_DAIWA_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'ooedo' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('CUSTOMIZE_OOEDO_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'first_cabin' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('CUSTOMIZE_FIRST_CABIN_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'unizo' => [
            'driver' => env('AWS_DRIVER', 'mysql'),
            'host' => env('AWS_HOST', '127.0.0.1'),
            'port' => env('AWS_PORT', '3306'),
            'database' => env('CUSTOMIZE_UNIZO_DATABASE', 'forge'),
            'username' => env('AWS_USERNAME', 'forge'),
            'password' => env('AWS_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
