<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/check', 'TokyustayController@Check');
/*
 * 年度別
 * */
Route::get('/valuechain-start', 'TokyustayController@ValuechainStart'); 
//1.クチコミ評価分析（評価点数）
Route::get('/tokyu-stay-report', 'TokyustayController@TokyuStayReport');
//2.クチコミ評価分析（バリューチェーン）
Route::get('/tokyu-stay-report-valuechain', 'TokyustayController@TokyuStayReportValuechain');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
